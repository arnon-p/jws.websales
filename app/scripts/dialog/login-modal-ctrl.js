"use strict";

angular.module('NwsFront').controller('loginModalCtrl', function($scope, $q, $timeout, constantService, authenService, loaderService, exceptionService, storageService, dialogService) {
	//declare variable
	$scope.bean = null;
    $scope.companyList = null;

    //declare function
    $scope.initForm = function() {
    	$scope.bean = {};

        $scope.companyList = constantService.getCompanyList();

        if(!$scope.companyList) {
            $timeout(function(){
                $scope.companyList = constantService.getCompanyList();
            }, 5000);
        }
    };
    $scope.doLogin = function(e) {
    	var deferred = $q.defer();

    	loaderService.show();

    	var reqObj = angular.copy($scope.bean);

    	//clear password
    	$scope.bean.userPassword = null;

    	authenService.login(reqObj, true).then(function(data) {
            dialogService.closeDialog('loginModal');

            constantService.getAllConstants();
            constantService.getConstantCompanyBigData();

        	loaderService.hide();

            $scope.redirectLanding(data.bean);
        }, function(error) {
            $('#loginModal').modal('hide');
            
        	loaderService.hide();

            exceptionService.handler($scope, error, function(){
                $('#loginModal').modal('show');
            });
            
            deferred.reject(error);
        });

        return deferred.promise;
    };

    //event
    // $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    // });
});