"use strict";
angular.module('NwsFront').controller('logoutModalCtrl', function($scope, $q, authenService, loaderService, exceptionService, $location, dialogService) {
    //declare variable


    //declare function
    $scope.doLogout = function(e) {
        var deferred = $q.defer();
        
        loaderService.show();

        var logoutPromise = authenService.logout();

        logoutPromise.then(function(data) {
            dialogService.closeDialog('logoutModal');

            $location.path('/app/front/home');

            deferred.resolve(data);

            loaderService.hide();
        }, function(error) {
            exceptionService.handler($scope, error);

            deferred.reject(error);

            loaderService.hide();
        });

        return deferred.promise;
    };
});