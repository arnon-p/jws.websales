"use strict";

angular.module('NwsFront').service('exceptionService', function(dialogService) {
    var handler = function(scope, err, okFnc) {
        let error = err.data ? err.data : err;
        let status = error.status;
        // let msg = error.data ? error.data.errorMessage : error.errorMessage;
        let msg = error.data ? error.data.errorMessage : undefined;

        if(!msg){
            msg = 'msg.error.';
            if (status && status === -1) {
                // ERR_CONNECTION_REFUSED
                msg = msg + 'err_connection_refused';
            } else if (status) {
                // business exception 
                status = status.toString();
                msg = msg + status.toLowerCase();
            } else {
                //internal error
                msg = msg + 'internal_error';
            }
        }

        dialogService.showErrorMsgByKey(scope, msg, okFnc);
    };

    var queryHandler = function(scope, err) {
        let error = err.data;
        if (!error.status || error.status && error.status !== 'NOT_FOUND') {
            handler(scope, error);
        } else {
            // NOTHING
        }
    };

    return {
        handler: handler,
        queryHandler: queryHandler
    };
});
