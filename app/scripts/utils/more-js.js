//for js

window.unbindRowCheckbox = function(ev){
	try {
		var parents = $('input[type=checkbox]').parent();

		if(parents.length>0){
			parents.each(function(index, element) {
			    if('td'===element.localName){
			    	$(this).click(function(e){
			    		e.stopPropagation();
			    	});
			    }
			});
		}
	}catch(e){
		console.log('unbindRowCheckbox' + e);
	}
	
};

window.bindCounTo = function(ev){
    $(function() {
        "use strict";
		$(".fact-number").appear(function(){
            var dataperc = $(this).attr('data-perc');
			$(this).each(function(){			
				$(this).find('.factor').delay(6000).countTo({
					from: 10,
					to: dataperc,
					speed: 3000,
					refreshInterval: 50,	
				});  
			});
		});
	});
};

window.isJson = function(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}