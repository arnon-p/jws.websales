"use strict";
//jQuery boostrap modal
angular.module('NwsFront').factory('dialogService', function($filter) {
    
	var modalOptions = {'backdrop': 'static'};

	var openDialog = function(dialogId, scope, bean, okFnc, afterOpenFnc, afterHideFnc) {
		if(!dialogId){
			console.log('Error null dialogId.');

			return;
		}

		if(!scope){
			console.log('Error null scope.');

			return;
		}

		try{
			scope.modal.bean = bean;
			scope.modal.okFnc = okFnc;

			if(afterOpenFnc){
				$('#' + dialogId).on('shown.bs.modal', afterOpenFnc);
			}

			if(afterHideFnc){
				$('#' + dialogId).on('hidden.bs.modal', afterHideFnc);
			}

			//clear modal bean on close
			$('#' + dialogId).on('hide.bs.modal', function(){
				scope.modal.bean = null;
				scope.modal.okFnc = null;
			});

			//clear modal init event on close
			$('#' + dialogId).on('hidden.bs.modal', function(){
				$('#' + dialogId).off('shown.bs.modal');
			});

			$('#' + dialogId).modal(modalOptions);
		}catch(e){
			console.log('Error openDialog id #' + dialogId + ' : ' + e);
		}
	};

	var closeDialog = function(dialogId, afterHideFnc){
		if(!dialogId){
			console.log('Error null dialogId.');

			return;
		}

		if(afterHideFnc){
			$('#' + dialogId).on('hidden.bs.modal', afterHideFnc);
		}

		$('#' + dialogId).on('hidden.bs.modal', function(){
			$('#' + dialogId).off('hidden.bs.modal');
		});

		$('#' + dialogId).off('shown.bs.modal');

		$('#' + dialogId).modal('hide');
	};

	var showAlertDialog = function(scope, title, message){
		var bean = {
			title: title,
			message: message
		};

		openDialog('alertModal', scope, bean);
	};

	var showAlertDialogByMsgKey = function(scope, msgKey,okFnc){
		var title = $filter('translate')('common.warning');
		var message = $filter('translate')(msgKey);
		
		var bean = {
			title: title,
			message: message
		};

		openDialog('alertModal', scope, bean,null,null,okFnc);
	};

	var showInfoDialog = function(scope, title, message){
		var bean = {
			title: title,
			message: message
		};

		openDialog('infoModal', scope, bean);
	};

	var showErrorDialog = function(scope, title, message, okFnc){
		var bean = {
			title: title,
			message: message
		};
		
		openDialog('errorModal', scope, bean, okFnc);
	};

	var showConfirmDialog = function(scope, title, message, okFnc){
		var bean = {
			title: title,
			message: message
		};

		openDialog('confirmModal', scope, bean, okFnc);
	};

	var showConfirmDialogMsgKey = function(scope, msgKey, okFnc){

		var title = $filter('translate')('common.warning');
		var msg = $filter('translate')(msgKey);

		var bean = {
			title: title,
			message: msg
		};

		openDialog('confirmModal', scope, bean, okFnc);
	};

	var confirmDeleteDialog = function(scope, okFnc){
		var title = $filter('translate')('common.warning');
		var msg = $filter('translate')('common.confirmdelete');

		showConfirmDialog(scope, title, msg, okFnc);
	};

	var confirmDeleteDialogWithMessage = function(scope, okFnc,messageKey){
		var title = $filter('translate')('common.warning');
		var msg = $filter('translate')(messageKey);
		showConfirmDialog(scope, title, msg, okFnc);
	};

	var closeDeleteDialog = function(){
		closeDialog('confirmModal');
	};

	var showCommonErrorDialog = function(scope, message, okFnc){
		var title = $filter('translate')('common.error');

		showErrorDialog(scope, title, message, okFnc);
	};

	var showErrorMsgByKey = function(scope, msgKey, okFnc) {
        var message = $filter('translate')(msgKey);

        return showCommonErrorDialog(scope, message, okFnc);
    };

    var saveSuccess = function(scope) {
    	var title = $filter('translate')('common.success');
    	var msg = $filter('translate')('common.savesuccess');
		
    	showInfoDialog(scope, title, msg);
	};
	
	var saveSuccessWithFunction = function(scope,afterHideFnc) {
    	var title = $filter('translate')('common.success');
		var msg = $filter('translate')('common.savesuccess');
		
		var bean = {
			title: title,
			message: msg
		};

		openDialog('infoModal', scope, bean, null,null, afterHideFnc);
    };

    var deleteSuccess = function(scope) {
    	var title = $filter('translate')('common.success');
    	var msg = $filter('translate')('common.deletesuccess');

    	showInfoDialog(scope, title, msg);
    };

    var warningSelectRecord = function(scope){
    	var title = $filter('translate')('common.warning');
    	var msg = $filter('translate')('common.selectone');

    	showAlertDialog(scope, title, msg);
    };

    var alertOnModal = function(scope, currentModalName, title, message) {
        closeDialog(currentModalName, function(){

            var bean = {
                title: title,
                message: message
            };

            openDialog('errorModal', scope, bean, null, null, function(){
                openDialog(currentModalName, scope);
            });
        });
    };
    
    return {
        openDialog: openDialog,
        closeDialog: closeDialog,
        showAlertDialog: showAlertDialog,
        showErrorDialog: showErrorDialog,
		showConfirmDialog: showConfirmDialog,
		showConfirmDialogMsgKey: showConfirmDialogMsgKey,
        showCommonErrorDialog: showCommonErrorDialog,
        showErrorMsgByKey: showErrorMsgByKey,
        showInfoDialog: showInfoDialog,
		saveSuccess: saveSuccess,
		saveSuccessWithFunction: saveSuccessWithFunction,
        deleteSuccess: deleteSuccess,
		warningSelectRecord: warningSelectRecord,
		confirmDeleteDialog: confirmDeleteDialog,
		confirmDeleteDialogWithMessage: confirmDeleteDialogWithMessage,
		closeDeleteDialog: closeDeleteDialog,
		showAlertDialogByMsgKey: showAlertDialogByMsgKey,
		alertOnModal: alertOnModal
    };
});