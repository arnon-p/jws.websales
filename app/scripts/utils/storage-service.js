"use strict";

angular.module('NwsFront').factory('storageService', function(localStorageService, $q, dateTimeUtils) {
    var setDataAsync = function(dataKey, dataValue) {
        let data = convertDateToTime(dataValue);

        return $q(function(resolve, reject) {
            try {
                setData(dataKey, data);
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    };
    var getDataAsync = function(dataKey) {
        return $q(function(resolve, reject) {
            try {
                let dataValue = getDataAsync(dataKey);

                // let data = convertTimeToDate(dataValue);

                resolve(dataValue);
            } catch (e) {
                reject(e);
            }
        });
    };
    var setData = function(dataKey, dataValue) {
        // let data = convertDateToTime(dataValue);

        localStorageService.set(dataKey, dataValue);
    };
    var getData = function(dataKey) {
        let dataValue = localStorageService.get(dataKey);

        // let data = convertTimeToDate(dataValue);

        return dataValue;
    };
    var convertDateToTime = function(input){

        if(!input){
            return null;
        }

        let result = {};

        angular.forEach(input, function(value, key){

            let loopResult = null;

            if(angular.isObject(value)){
                if(window.isJson(value)){
                    loopResult = convertDateToTime(value);
                }else{
                    loopResult = dateTimeUtils.dateToValue(value);
                }
            }else{
                loopResult = value;
            }

            result[key] = loopResult;
        });

        return result;
    };
    var convertTimeToDate = function(input){

        if(!input){
            return null;
        }

        let result = {};

        angular.forEach(input, function(value, key){

            let loopResult = null;

            if(window.isJson(value)){
                loopResult = convertTimeToDate(value);
            }else{
                loopResult = dateTimeUtils.valueToDate(value);
            }

            result[key] = loopResult;
        });

        return result;
    };

    return {
        getData: getData,
        setData: setData,
        setDataAsync: setDataAsync,
        getDataAsync: getDataAsync
    };
});