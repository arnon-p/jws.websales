"use strict";

angular.module('NwsFront').service('httpService', function($http, $q, appEndpoint, storageService, localStorageConfig, $base64, $timeout) {

	var httpPost = function(servicePath, data, isDataResponse) {

		let token = getToken();
		// let token = '1234';
		
		let deferred = $q.defer();

		let reqOpt = {
			method : 'POST',
			url : appEndpoint.baseUrl + servicePath,
			headers : {
				'Authorization' : 'Bearer ' + token,
				'Content-Type' : 'application/json; charset=utf-8',
				'a-token' : token
			},
			data : JSON.stringify(data),
			dataType : 'json'
		};

		if(isDataResponse){
			reqOpt.responseType = 'arraybuffer';
		}

		$http(reqOpt).then(function(data, status, headers, config) {
			

			if(isDataResponse){
				let blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
			    let objectUrl = URL.createObjectURL(blob);
			    window.open(objectUrl);

				deferred.resolve(status);
			}else{
				deferred.resolve(data.data);
			}

			// $timeout(function(){
			// 	unbindRowCheckbox();
			// }, 500);
		}, function(data, status, headers, config) {
			let err = {
				data : !isDataResponse ? data : null,
				status : status,
				headers : headers,
				config : config
			};
			
			deferred.reject(err);
		});

		return deferred.promise;
	};

	var bussinessPost = function(servicePath, reqObj, showLoading, isDataResponse) {
		let deferred = $q.defer();

		let httpPromise = httpPost(servicePath, reqObj, showLoading, isDataResponse);

		httpPromise.then(function(response){
			switch (response.status){
				case 'SUCCESS' :
					deferred.resolve(response);
					break;
				default : 
					deferred.reject(response);

					break;
				}
		}, function(error){
			deferred.reject(error);
		});

		return deferred.promise;
	};

	var getToken = function() {
		let token = storageService.getData(localStorageConfig.userToken);
        if (!token) {
            token = 'd2lraTpwZWRpY==='; //simple token
        } else {
            token = $base64.decode(token);
        }
        return token;
	};
	
	var httpPostMultipart = function(servicePath, data) {

		var token = getToken();
		// let token = '1234';

		let deferred = $q.defer();
		
		$http({
			method : 'POST',
			url : appEndpoint.baseUrl + servicePath,
			headers : {
				'Authorization' : 'Bearer ' + token,
				'Content-Type' : undefined,
				// 'Content-Type' : 'application/x-www-form-urlencoded',
				// 'Accept-Encoding' : 'gzip, deflate',
				'a-token' : token
			},
			data : data,
			dataType : 'json',
			transformRequest: angular.identity,
//			transformResponse: transformResponse
		}).then(function(data) {
			deferred.resolve(data.data);
		}, function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	};
	
	var transformResponse = function(data, headers, status){
		if (angular.isString(data)) {
			try{	
				// Strip json vulnerability protection prefix and trim whitespace
				let tempData = data.replace(JSON_PROTECTION_PREFIX, '').trim();

				if (tempData) {
					let contentType = headers('Content-Type');
					if ((contentType && (contentType.indexOf(APPLICATION_JSON) === 0)) || isJsonLike(tempData)) {
						data = angular.fromJson(tempData);
					}
				}
			} catch (e) {
				console.log('Error Transform response : ' + e);
			}
		}

		return data;
	};

	return {
		asyncPost : function(servicePath, data) {
			return httpPost(servicePath, data);
		},
		asyncPostBg : function(servicePath, data) {
			return httpPost(servicePath, data);
		},
		commonPost : function(servicePath, data) {
			return bussinessPost(servicePath, data);
		},
		commonPostBg : function(servicePath, data) {
			return bussinessPost(servicePath, data);
		},
		postWithDataResponse: function(servicePath, data) {
			return httpPost(servicePath, data, true);
		},
		asyncPostMultipart : function(servicePath, data) {
			return httpPostMultipart(servicePath, data);
		}
	};
});