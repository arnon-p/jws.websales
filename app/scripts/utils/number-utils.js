angular.module('NwsFront').service('numberUtils', function() {
    var fixedDecimalPlaces = function(value, decimalPlaces) {
    	if(!decimalPlaces){
    		decimalPlaces = 0;
    	}
        
        let result = math.round(value, decimalPlaces + 5);

        result = math.round(result, decimalPlaces);

        // if(!round){
        //     round = 'up';  //default by business SAP
        // }

        // switch(round){
        //     case 'up':
        //         let roundUnit = Math.pow(10, decimalPlaces);

        //         let roundedUp = Math.ceil(value * 100) / 100;

        //         result = Number(Math.round(parseFloat(roundedUp + 'e' + decimalPlaces)) + 'e-' + decimalPlaces);

        //         break;
        //     default:
        //         result = Number(Math.round(parseFloat(value + 'e' + decimalPlaces)) + 'e-' + decimalPlaces);

        //         break;
        // }

        return result;
    };
    
    return {
        fixedDecimalPlaces: fixedDecimalPlaces
    };
});
