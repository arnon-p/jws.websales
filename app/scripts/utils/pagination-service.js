'use strict';
angular.module('NwsFront').service('Pagination', function() {
	
	var getDefaultPaginable = function () {
		return {
			currentPage: 1,
			offset: 0,
            rowPerPage: 10,
            rowCount: 0
		};
	};
	
	var getPaginable = function(paginable, nextPage) {
		if (!paginable) {
			paginable = getDefaultPaginable();
		} 
		paginable.offset = paginable.rowPerPage * (paginable.currentPage-1);

		if(nextPage){
			paginable.currentPage = nextPage;
		}
		
		return paginable;
	};
	
	// ไม่มีแล้ว เนื่องจากไปเพิ่ม attribue ที่ paginable dto
	var setPaginable = function(reqPaginable, resPaginable) {
		resPaginable.pageNumber = reqPaginable.pageNumber;
		return resPaginable;
	};
	
	return {
		getDefaultPaginable: getDefaultPaginable,
		getPaginable: getPaginable,
		setPaginable: setPaginable
	};
	
});