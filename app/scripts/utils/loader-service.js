"use strict";

//for bootrap loader in index.html name pageloader , softloader
angular.module('NwsFront').factory('loaderService', function($cacheFactory, localStorageConfig, $q, $timeout) {
  
	var loadingPromiseId = 'loading-promise';

	var cacheFactory = $cacheFactory(localStorageConfig.loadingCacheId);

	var isLoading = function() {
		var cnt = cacheFactory.get(loadingPromiseId);

		return angular.isDefined(cnt) && cnt !== 0;
	};

	var addMoreLoading = function() {
		var cnt = cacheFactory.get(loadingPromiseId);

		if(angular.isUndefined(cnt)){
			cnt = 0;
		}

		++cnt;

		cacheFactory.put(loadingPromiseId, cnt);
	};

	var removeMoreLoading = function() {
		var cnt = cacheFactory.get(loadingPromiseId);

		if(angular.isDefined(cnt)){
			if(cnt>0){
				--cnt;

				cacheFactory.put(loadingPromiseId, cnt);
			}else{
				cacheFactory.put(loadingPromiseId, 0);
			}
		}
	};

	var show = function(isHard){
		if(isHard){
			$('#pageloader').show();
			$('#pageloader').find('.loader-item').show();
		}else{
			$('#softloader').show();
			// $('#softloader').find('.loader-item').show();
		}
	};

	var hide = function() {
		$('#pageloader').hide();
		// $('.loader-item').hide();
		$('#softloader').hide();
	};

	var showLoading = function(isHard) {
    	if(!isLoading()){
    		show(isHard);
    	}

    	addMoreLoading();
    };

    var hideLoading = function() {
    	$timeout(function(){
    		removeMoreLoading();

			if(!isLoading()){
	    		hide();
	    	}    		
    	}, 500);
    };

    return {
      show: showLoading,
      hide: hideLoading
    };
});