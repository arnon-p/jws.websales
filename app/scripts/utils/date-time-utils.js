"use strict";
angular.module('NwsFront').service('dateTimeUtils', function() {
    var addDay = function(date, addDay) {

        if(date && addDay){
            var result = angular.copy(date);

            result.setDate(date.getDate() + addDay);

            return result;
        }

        return null;
    };
    var dateToValue = function(date){
        if(date && angular.isDate(date)) {
            return date.getTime();
        }else{
            return date;
        }
    }
    var valueToDate = function(val){
        if(val && angular.isNumber(val)){
            return new Date(val);
        }else{
            return val;
        }
    }
    return {
        currentDate: function() {
            return new Date();
        },
        addDay: addDay,
        dateToValue: dateToValue,
        valueToDate: valueToDate
    };
});
