"use strict";

angular.module('NwsFront').controller('appCtrl', function($scope, exceptionService, $q, authenService, dateTimeUtils, $location, loaderService, appEndpoint, $timeout, $state, $window, $translate, constantService) {

    //--common dialog--\\
    $scope.modal = {
        bean: null,
        okFnc: null
    };
    //--common dialog--\\
    $scope.userObj = null;

    //declare function
    $scope.gotoState = function(state) {
        $state.go(state);
    };
    $scope.getScopeById = function(id) {
        return angular.element($("#" + id)).scope();
    };
    $scope.checkAll = function(checked, list) {
        if (list) {
            angular.forEach(list, function(value, key) {
                value.checked = checked;
            });
        }
    };
    $scope.onHref = function(link) {
        $window.open(link, '_blank');
    };
    $scope.downloadReport = function(filename){
        const downloadUrl = appEndpoint.baseUrl + '/common/getReportFiles/';

        let reportUrl = downloadUrl + filename + '?v=' + dateTimeUtils.currentDate().getTime();

        $scope.onHref(reportUrl);
    };

    $scope.changeLanguage = function(lang) {
        if (!lang) {
            return;
        }

        //process change language
        $scope.applyChangeLanguage(lang);
    };

    $scope.applyChangeLanguage = function(lang) {
        $translate.use(lang);
    };

    $scope.redirectLanding = function (userData) {
        loaderService.show(true);

        // constantService.getAllConstants();

        $scope.userObj = userData;

        // $scope.applyUserLanguage();

        let userLevel = userData.userLevel;

        let path = null;

        switch(userLevel){
            case 'U' :
                path = '/app/sales/dashboard';
                
                break;
            case 'M' :
                path = '/app/sales/dashboard';
                
                break;
            case 'A' :
                path = '/app/sales/dashboard';
                
                break;
            case 'S' :
                path = '/app/sales/dashboard';
                
                break;
            default :
                path = '/app/front/home';

                break;
        }

        $location.path(path);

        loaderService.hide();
    };

    $scope.doLogout = function (e) {
        let deferred = $q.defer();

        loaderService.show();

        authenService.logout().then(function(res){
            loaderService.hide();

            $location.path('/app/front/home');

            $timeout(function() {
                $window.location.reload();
            }, 400);
        }, function(err){
            loaderService.hide();

            exceptionService.handler($scope, error);
        });

        // $scope.gotoState('app.front.home');
        return deferred.promise;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $timeout(function() {
            initParticles2();
        }, 500);

    });

})
.controller('frontCtrl', function($scope) {

    //declare variable

    //declare function

    //event
})
.controller('salesCtrl', function($scope, userLocalService) {

    //declare variable
    $scope.userDisplay = null;
    $scope.orderQtyDecimalPlace = 4;

    //declare function
    $scope.isAllowModule = function(e, stateModule) {
        if(userLocalService.isLoggedIn()){
            let userObj = userLocalService.getUserObj();

            let moduleList = userObj.moduleList;

            if(!moduleList){
                return false;
            }

            if(!stateModule){
                return false;
            }

            for(let i=0;i<moduleList.length;i++){
                let module = moduleList[i];

                if(stateModule===module.moduleCode){
                    return true;
                }
            }
        }

        return false;
    };

    $scope.isAllowMenu = function(e, stateModuleList){
        if(userLocalService.isLoggedIn()){
            let userObj = userLocalService.getUserObj();

            let moduleList = userObj.moduleList;

            if(!moduleList){
                return false;
            }

            if(!stateModuleList){
                return false;
            }

            for(let j=0;j<stateModuleList.length;j++){
                let stateModule = stateModuleList[j];

                for(let i=0;i<moduleList.length;i++){
                    let module = moduleList[i];

                    if(stateModule===module.moduleCode){
                        return true;
                    }
                }
            }
        }

        return false;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        if(userLocalService.isLoggedIn()){
            $scope.userDisplay = userLocalService.getUserObj().display;
        }
    });

});