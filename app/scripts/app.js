"use strict";

angular.module('NwsFront', ['ngAnimate', 'angularjs-dropdown-multiselect', 'ngTouch', 'ui.router', 'ngFileUpload', 'LocalStorageModule', 'alexjoffroy.angular-loaders', 'duScroll', 'base64', 'pascalprecht.translate', 'bw.paging', 'autoCompleteModule'])

.config(function(localStorageServiceProvider) {
	localStorageServiceProvider
		.setPrefix('NwsFront')
		.setStorageType('sessionStorage')
		.setNotify(true, true);
})

.config(['$translateProvider', function($translateProvider) {
	$translateProvider.useSanitizeValueStrategy('escape');
	$translateProvider.useStaticFilesLoader({
		prefix: 'translation/translate-',
		suffix: '.json'
	}).preferredLanguage('en');
}])

.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'views/main.html',
		controller: 'appCtrl'
	}).state('app.front', {
		url: '/front',
		abstract: true,
		views: {
			'mainContent': {
				templateUrl: 'views/front.html',
				controller: 'frontCtrl'
			}
		}
	}).state('app.front.home', {
		url: '/home',
		views: {
			'frontContent': {
				templateUrl: 'views/front/home.html',
				controller: 'homeCtrl'
			}
		},
		data: {
			moduleCode: 'FH001'
		}
	}).state('app.sales', {
		url: '/sales',
		abstract: true,
		views: {
			'mainContent': {
				templateUrl: 'views/sales.html',
				controller: 'salesCtrl'
			}
		}
	}).state('app.sales.dashboard', {
		url: '/dashboard',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/dashboard.html',
				controller: 'salesDashboardCtrl'
			}
		}
	}).state('app.sales.bpmastersearch', {
		url: '/bpmastersearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/bp/master-search.html',
				controller: 'bpMasterSearchCtrl'
			}
		},
		data: {
			moduleCode: 'BP001'
		}
	}).state('app.sales.bpmasteredit', {
		url: '/bpmasteredit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/bp/master-edit.html',
				controller: 'bpMasterEditCtrl'
			}
		},
		data: {
			moduleCode: 'BP001'
		}
	}).state('app.sales.invbatchnumberreportsearch', {
		url: '/invbatchnumberreportsearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/batch-number-report-search.html',
				controller: 'batchNumberReportSearchCtrl'
			}
		},
		data: {
			moduleCode: 'IB001'
		}
	}).state('app.sales.invbatchnumberreportview', {
		url: '/invbatchnumberreportview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/batch-number-report-view.html',
				controller: 'batchNumberReportViewCtrl'
			}
		},
		data: {
			moduleCode: 'IB001'
		}
	}).state('app.sales.invbatchnumberdocview', {
		url: '/invbatchnumberdocview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/batch-number-doc-view.html',
				controller: 'batchNumberDocViewCtrl'
			}
		},
		data: {
			moduleCode: 'IB001'
		}
	}).state('app.sales.invmastersearch', {
		url: '/invmastersearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/master-search.html',
				controller: 'invMasterSearchCtrl'
			}
		},
		data: {
			moduleCode: 'IM001'
		}
	}).state('app.sales.invmasteredit', {
		url: '/invmasteredit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/master-edit.html',
				controller: 'invMasterEditCtrl'
			}
		},
		data: {
			moduleCode: 'IM001'
		}
	}).state('app.sales.invstatussearch', {
		url: '/invstatussearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/status-search.html',
				controller: 'invStatusSearchCtrl'
			}
		},
		data: {
			moduleCode: 'IS001'
		}
	}).state('app.sales.invstatusedit', {
		url: '/invstatusedit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/status-edit.html',
				controller: 'invStatusEditCtrl'
			}
		},
		data: {
			moduleCode: 'IS001'
		}
	}).state('app.sales.invstatusdoc', {
		url: '/invstatusdoc',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/status-doc-view.html',
				controller: 'invStatusDocViewCtrl'
			}
		},
		data: {
			moduleCode: 'IS001'
		}
	}).state('app.sales.invpurchasesearch', {
		url: '/invpurchasesearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/purchase-search.html',
				controller: 'purchaseReportSearchCtrl'
			}
		},
		data: {
			moduleCode: 'IP001'
		}
	}).state('app.sales.invpurchaseview', {
		url: '/invpurchaseview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/inv/purchase-view.html',
				controller: 'purchaseReportViewCtrl'
			}
		},
		data: {
			moduleCode: 'IP001'
		}
	}).state('app.sales.arpurchasecostsearch', {
		url: '/arpurchasecostsearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/purchase-cost-search.html',
				controller: 'purchaseReportCostSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AP001'
		}
	}).state('app.sales.arpurchasecostview', {
		url: '/arpurchasecostview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/purchase-cost-view.html',
				controller: 'purchaseReportCostViewCtrl'
			}
		},
		data: {
			moduleCode: 'AP001'
		}
	}).state('app.sales.arageingreportsearch', {
		url: '/arageingreportsearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/ageing-report-search.html',
				controller: 'ageingReportSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AG001'
		}
	}).state('app.sales.arageingreportview', {
		url: '/arageingreportview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/ageing-report-view.html',
				controller: 'ageingReportViewCtrl'
			}
		},
		data: {
			moduleCode: 'AG001'
		}
	}).state('app.sales.aranalysisreportsearch', {
		url: '/aranalysisreportsearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/analysis-report-search.html',
				controller: 'analysisReportSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AS001'
		}
	}).state('app.sales.aranalysisreportedit', {
		url: '/aranalysisreportedit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/analysis-report-view.html',
				controller: 'analysisReportViewCtrl'
			}
		},
		data: {
			moduleCode: 'AS001'
		}
	}).state('app.sales.arbackordersearch', {
		url: '/arbackordersearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/back-order-search.html',
				controller: 'backOrderSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AB001'
		}
	}).state('app.sales.arbackorderedit', {
		url: '/arbackorderedit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/back-order-edit.html',
				controller: 'backOrderEditCtrl'
			}
		},
		data: {
			moduleCode: 'AB001'
		}
	}).state('app.sales.arordersearch', {
		url: '/arordersearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/order-search.html',
				controller: 'orderSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AO001'
		}
	}).state('app.sales.arorderedit', {
		url: '/arorderedit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/order-edit.html',
				controller: 'orderEditCtrl'
			}
		},
		data: {
			moduleCode: 'AO001'
		}
	}).state('app.sales.aronhandsearch', {
		url: '/aronhandsearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/on-hand-search.html',
				controller: 'onHandSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AH001'
		}
	}).state('app.sales.aronhandcostsearch', {
		url: '/aronhandcostsearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/on-hand-cost-search.html',
				controller: 'onHandCostSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AH002'
		}
	}).state('app.sales.aronhandview', {
		url: '/aronhandview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/on-hand-view.html',
				controller: 'onHandViewCtrl'
			}
		},
		data: {
			moduleCode: 'AH001'
		}
	}).state('app.sales.arorderstatussearch', {
		url: '/arorderstatussearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/order-status-search.html',
				controller: 'orderStatusSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AO002'
		}
	}).state('app.sales.arorderstatusview', {
		url: '/arorderstatusview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/order-status-view.html',
				controller: 'orderStatusViewCtrl'
			}
		},
		data: {
			moduleCode: 'AO002'
		}
	}).state('app.sales.brochureview', {
		url: '/brochureview',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/brochure-view.html',
				controller: 'brochureViewCtrl'
			}
		},
		data: {
			moduleCode: 'BC001'
		}
	}).state('app.sales.arorderapprovesearch', {
		url: '/arorderapprovesearch',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/order-approve-search.html',
				controller: 'orderApproveSearchCtrl'
			}
		},
		data: {
			moduleCode: 'AP002'
		}
	}).state('app.sales.arorderapproveedit', {
		url: '/arorderapproveedit',
		views: {
			'salesContent': {
				templateUrl: 'views/sales/ar/order-approve-edit.html',
				controller: 'orderApproveEditCtrl'
			}
		},
		data: {
			moduleCode: 'AP002'
		}
	});

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/front/home');

})
.run(function(constantService, $timeout){

	// $timeout(function(){
		constantService.getPublicConstants();
	// }, 500);

	// $timeout(function(){
		constantService.getAllConstants();
		constantService.getConstantCompanyBigData();
	// }, 500);
	
})
.run(function($rootScope, $timeout, $state, $window, $document, $location, userLocalService) { //state event

	//check online session
	$rootScope.$on('$locationChangeStart', function(e, newUrl, oldUrl, newState, oldState) {
		//handle before change

		//state policy
		{
			if(newUrl.indexOf('/app/front')===-1){
				if(userLocalService.isLoggedIn()){
					let userObj = userLocalService.getUserObj();

					let userLevel = userObj.userLevel;
					switch(userLevel){
						case 'U' :
							//validate role
							if(newUrl.indexOf('/app/sales')===-1){
								$rootScope.invalidAuth(e);
							}else{
								//valid from role validate

								//chain module check
								let canAccess = $rootScope.moduleAccessCheck(e, newUrl, userObj.moduleList);

								if(!canAccess){
									$rootScope.invalidAuth(e);
								}
							}

							break;
						default :
							$rootScope.invalidAuth(e);

							break;
					}
				}else{
					$rootScope.invalidAuth(e);
				}
			}else{
				//do nothing
			}
		}
	});

	$rootScope.moduleAccessCheck = function(e, newUrl, moduleList) {

		let pathIndex = newUrl.indexOf('#!');

		let newPath = newUrl.substring(pathIndex + 3, newUrl.length);

		// let pass = moduleList ? true : false;	//TODO skip check when null for test
		let pass = false;

		let newState = newPath.replace('/', '.');
		newState = newState.replace('/', '.');

		// console.log('newState : ' + newState);

		let state = $state.get(newState);

		if(!state){
			return true;	//skip when state undefined
		}

		let stateData = state.data;

		if(!stateData){
			return true;	//skip when state undefined
		}

		let newStateCode = stateData.moduleCode;

		if(!newStateCode){
			return true;
		}

		// console.log('newStateCode : ' + newStateCode);

		if(newStateCode && moduleList){
			for(let i=0;i<moduleList.length;i++){
				let module = moduleList[i];

				if(newStateCode===module.moduleCode){
					pass = true;

					break;
				}
			}
		}

		return pass;
	};

	$rootScope.invalidAuth = function(e){
		// e.preventDefault();

		$location.path('/app/front/home');

		$timeout(function() {
			// $location.path('/app/front/home');
			// $window.location.href = '/#!/app/front/home';
			$window.location.reload();
		}, 400);
		// $location.path('/app/front/home');
	};

	$rootScope.$on('$locationChangeSuccess', function(e, newUrl, oldUrl, newState, oldState) {
		//handle changed state

		$timeout(function() {
			$document.scrollTop(0, 1000);
		}, 500);
	});
}) //duScroll setting
.value('duScrollDuration', 1500);