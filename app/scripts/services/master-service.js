"use strict";

angular.module('NwsFront').service('masterService', function(httpService, userLocalService) {
    
    var serviceEndPoint = '/master';

    var searchBp = function(reqObj) {
    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchBusinessPartnerByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var getBpByCardCode = function(id) {
		var reqObj = {'cardCode': id};

		reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchBusinessPartnerByCardCode', {'Condition': reqObj}).then(function(response) {
			return response.response;
		});
	};

	var addBp = function(reqObj) {

		reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/createBusinessPartner', {'Data': reqObj}).then(function(response) {
			return response;
		});
	};

	var editBp = function(reqObj) {

		reqObj.Company = userLocalService.getCompany();
		
		return httpService.commonPost(serviceEndPoint + '/updateBusinessPartner', {'Data': reqObj}).then(function(response) {
			return response;
		});
	};
	
	//--Inventory--\\
	var searchItem = function(reqObj) {
    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchItemMasterByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var getItemByCode = function(id) {
		var reqObj = {'itemCode': id};

		reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchItemMasterByItemCode', {'Condition': reqObj}).then(function(response) {
			return response.response;
		});
	};

	var searchAutoBp = function(code, cardType) {
		let reqObj = {};

		reqObj.cardCode = code + '*';
		reqObj.Company = userLocalService.getCompany();

		if(cardType){
			reqObj.cardType = cardType;
		}

		return httpService.commonPost(serviceEndPoint + '/searchBusinessPartnerCriteria', {'condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchAutoItem = function(code){
		let reqObj = {};

		reqObj.itemCode = code + '*';
		reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchItemMasterCriteria', {'condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

    return {
		searchBp: searchBp,
		getBpByCardCode: getBpByCardCode,
		addBp: addBp,
		editBp: editBp,
		searchItem: searchItem,
		getItemByCode: getItemByCode,
		searchAutoBp: searchAutoBp,
		searchAutoItem: searchAutoItem
	};
	
});