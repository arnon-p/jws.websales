"use strict";

angular.module('NwsFront').service('authenService', function($q, appConfig, userLocalService, httpService) {
    
    var serviceEndPoint = '/common';

    var login = function(user, skipClear) {
        let deferred = $q.defer();

        if(!skipClear){
            userLocalService.clearUserData();
        }

        // user.Company = appConfig.Company;   //TODO

        var httpPromise = httpService.asyncPost(serviceEndPoint + '/login', user);
        httpPromise.then(function(resp) {
            let response = resp.response;

            if(!response){
                deferred.reject(resp);

                return;
            }

            switch (response.status) {
                case 'SUCCESS':
                case 'AU_DU':
                    userLocalService.setUserObj(response.bean);
                    userLocalService.setToken(response.token);
                    userLocalService.setCompany(response.company);

                    deferred.resolve(response);
                    break;
                default:
                    deferred.reject(response);
                    break;
            }
        }, function(error) {

            deferred.reject(error);
        });

        return deferred.promise;
    };

    var logout = function() {
        var deferred = $q.defer();

        var user = userLocalService.getUserObj();
        if (user) {
            var httpPromise = httpService.asyncPost(serviceEndPoint + '/logout', user);

            httpPromise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });

            userLocalService.clearUserData();
        } else {
            deferred.resolve();
        }

        return deferred.promise;
    };

    var tokenSignon = function(token){
        var deferred = $q.defer();

        var httpPromise = httpService.asyncPost(serviceEndPoint + '/tokenSignon', {userPassword: token});
        httpPromise.then(function(response) {
            switch (response.status) {
                case 'SUCCESS':
                    userLocalService.setUserObj(response.bean);
                    userLocalService.setToken(response.token);

                    deferred.resolve(response);
                    break;
                default:
                    deferred.reject(response);
                    break;
            }
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

	return {
		logout: logout,
		login: login,
        tokenSignon: tokenSignon
	};
	
});