"use strict";

angular.module('NwsFront').service('documentService', function(dateTimeUtils, httpService, userLocalService) {
    
    var serviceEndPoint = '/document';

    var searchBpForSO = function(conn) {

    	let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();

    	if(reqObj.cardCode){
    		reqObj.cardCode = reqObj.cardCode + '*';
    	}

    	if(reqObj.cardName){
    		reqObj.cardName = reqObj.cardName + '*';
    	}

		return httpService.commonPost(serviceEndPoint + '/searchBusinessPartnerForSO', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchItemForSO = function(con) {

		let reqObj = {};//angular.copy(con);
		
    	reqObj.Company = userLocalService.getCompany();
    	reqObj.Date = dateTimeUtils.dateToValue(con.docDate);
    	reqObj.CardCode = con.cardCode;

    	if(con.itemCode){
    		reqObj.ItemCode = con.itemCode + '*';
    	}

    	if(con.itemName){
    		reqObj.ItemName = con.itemName + '*';
    	}
    	
    	reqObj.PriceListNum = con.priceListNum;

    	if(con.bpItemName){
    		reqObj.BpItemName = con.bpItemName + '*';
    	}
    	
    	reqObj.Paginable = con.paginable;

		return httpService.commonPost(serviceEndPoint + '/searchItemMasterForSO', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchItemPrice = function(conn) {

    	let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();

    	reqObj.Date = dateTimeUtils.dateToValue(reqObj.Date);

		return httpService.commonPost(serviceEndPoint + '/searchItemPrice', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchBatchNumber = function(reqObj) {

    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchItemBatchNumberSelection', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchSalesPriceHist = function(reqObj) {

    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchSalesPriceHistoryByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchItemWhsOnHand = function(reqObj) {

    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchItemWhsOnHand', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchSeries = function(conn) {

    	let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();
    	reqObj.docDate = dateTimeUtils.dateToValue(reqObj.docDate);

		return httpService.commonPost(serviceEndPoint + '/searchSeriesByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var addNewSalesOrder = function(obj) {

		let reqObj = angular.copy(obj);

    	reqObj.Company = userLocalService.getCompany();
    	reqObj.IsDraft = false;

    	reqObj.docDate = dateTimeUtils.dateToValue(reqObj.docDate);
    	reqObj.docDueDate = dateTimeUtils.dateToValue(reqObj.docDueDate);
    	reqObj.taxDate = dateTimeUtils.dateToValue(reqObj.taxDate);

    	angular.forEach(reqObj.lines, function(line) {
    		line.docDueDate = dateTimeUtils.dateToValue(line.docDueDate);
    	});

		return httpService.commonPost(serviceEndPoint + '/createSalesOrder', {'Data': [reqObj]}).then(function(response) {
			if(response.response && response.response.length > 0){
				return response.response[0];
			}

			return null;
		});
	};

	var editSalesOrder = function(obj) {

		let reqObj = angular.copy(obj);
		
		reqObj.Company = userLocalService.getCompany();
    	// reqObj.IsDraft = false;

    	reqObj.docDate = dateTimeUtils.dateToValue(reqObj.docDate);
    	reqObj.docDueDate = dateTimeUtils.dateToValue(reqObj.docDueDate);
    	reqObj.taxDate = dateTimeUtils.dateToValue(reqObj.taxDate);

    	angular.forEach(reqObj.lines, function(line) {
    		line.docDueDate = dateTimeUtils.dateToValue(line.docDueDate);
    	});

		return httpService.commonPost(serviceEndPoint + '/updateSalesOrder', {'Data': [reqObj]}).then(function(response) {
			if(response.response && response.response.length > 0){
				return response.response[0];
			}

			return null;
		});
	};

	var searchOrder = function(conn) {

		let reqObj = angular.copy(conn);

		reqObj.isDraft = reqObj.draft ? true : false;
    	reqObj.Company = userLocalService.getCompany();

    	reqObj.docDateFrom = dateTimeUtils.dateToValue(reqObj.docDateFrom);
    	reqObj.docDateTo = dateTimeUtils.dateToValue(reqObj.docDateTo);
    	reqObj.docDueDateFrom = dateTimeUtils.dateToValue(reqObj.docDueDateFrom);
    	reqObj.docDueDateTo = dateTimeUtils.dateToValue(reqObj.docDueDateTo);

		return httpService.commonPost(serviceEndPoint + '/searchSalesOrderByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var getOrderByEntry = function(docEntry, code, isDraft) {

		let reqObj = {};

		reqObj.docEntry = docEntry;
		reqObj.Code = code;
    	reqObj.Company = userLocalService.getCompany();
    	reqObj.isDraft = isDraft;

		return httpService.commonPost(serviceEndPoint + '/searchSalesOrderByDocEntry', {'Condition': reqObj}).then(function(response) {

			if(response.response){
				let data = response.response;

				data.docDate = dateTimeUtils.valueToDate(data.docDate);
				data.docDueDate = dateTimeUtils.valueToDate(data.docDueDate);
				data.taxDate = dateTimeUtils.valueToDate(data.taxDate);

				if(data.isDraft){
					data.docNumDisp = data.docNum + ' (Draft)';
					// data.statusDisp = data.status + ' (' + data.wddStatus + ')';
				}else{
					data.docNumDisp = data.docNum;

					// if('Y'===data.canceled){
					// 	data.statusDisp = data.status + ' (Cancellation)';
					// }
				}
			}

			return response.response;
		});
	};

	var searchOrderRelation = function(reqObj) {

    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchSalesOrderDocumentRelation', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchOrderStatus = function(reqObj) {

    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchOrderStatusByCondition', {'Condition': reqObj}).then(function(response) {
			let resp = response.response;

			if(resp && resp.length===0){
				response.response = null;
			}else{
				angular.forEach(resp, function(docStatus){
					let soCreateTime = docStatus.soCreateTime;
					let doCreateTime = docStatus.doCreateTime;
					let invCreateTime = docStatus.invCreateTime;
					let rctCreateTime = docStatus.rctCreateTime;

					if(soCreateTime && soCreateTime.length>=4){
						docStatus.soCreateTime = soCreateTime[0] + soCreateTime[1] + ':' + soCreateTime[2] + soCreateTime[3];
					}

					if(doCreateTime && doCreateTime.length>=4){
						docStatus.doCreateTime = doCreateTime[0] + doCreateTime[1] + ':' + doCreateTime[2] + doCreateTime[3];
					}

					if(invCreateTime && invCreateTime.length>=4){
						docStatus.invCreateTime = invCreateTime[0] + invCreateTime[1] + ':' + invCreateTime[2] + invCreateTime[3];
					}

					if(rctCreateTime && rctCreateTime.length>=4){
						docStatus.rctCreateTime = rctCreateTime[0] + rctCreateTime[1] + ':' + rctCreateTime[2] + rctCreateTime[3];
					}
				});
			}
			return response;
		});
	};

	var searchApprovalOrder = function(conn) {

		let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();

    	reqObj.createDateFrom = dateTimeUtils.dateToValue(reqObj.createDateFrom);
    	reqObj.createDateTo = dateTimeUtils.dateToValue(reqObj.createDateTo);

		return httpService.commonPost(serviceEndPoint + '/searchApprovalSalesOrderByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var approveOrder = function(obj) {

		let reqObj = {};

		reqObj.docEntry = obj.docEntry;
    	reqObj.Company = userLocalService.getCompany();
    	reqObj.remark = obj.remark;
		reqObj.wddStatus = obj.wddStatus;
		reqObj.ParentKey = obj.parentKey;
    	reqObj.Code = obj.code;

		return httpService.commonPost(serviceEndPoint + '/approveSalesOrder', {'Data': [reqObj]}).then(function(response) {

			if(response.response){
				let data = response.response;

				data.docDate = dateTimeUtils.valueToDate(data.docDate);
				data.docDueDate = dateTimeUtils.valueToDate(data.docDueDate);
				data.taxDate = dateTimeUtils.valueToDate(data.taxDate);

				if(data.isDraft){
					data.docNumDisp = data.docNum + ' (Draft)';
				}else{
					data.docNumDisp = data.docNum;
				}
			}

			return response.response;
		});
	};

	var approveOrderList = function(list){

		let data = [];

		for(let i=0;i<list.length;i++){
			let obj = list[i];

			let reqObj = {};

			reqObj.docEntry = obj.docEntry;
			reqObj.Company = userLocalService.getCompany();
			reqObj.remark = obj.remark;
			reqObj.wddStatus = obj.wddStatus;
			reqObj.ParentKey = obj.parentKey;
			reqObj.Code = obj.code;

			data.push(reqObj);
		}
		
		return httpService.commonPost(serviceEndPoint + '/approveSalesOrder', {'Data': data}).then(function(response) {

			// if(response.response){
			// 	let data = response.response;

			// 	data.docDate = dateTimeUtils.valueToDate(data.docDate);
			// 	data.docDueDate = dateTimeUtils.valueToDate(data.docDueDate);
			// 	data.taxDate = dateTimeUtils.valueToDate(data.taxDate);

			// 	if(data.isDraft){
			// 		data.docNumDisp = data.docNum + ' (Draft)';
			// 	}else{
			// 		data.docNumDisp = data.docNum;
			// 	}
			// }

			return response.response;
		});
	};

	var searchGrossProfitByCondition = function(reqObj){
		reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/searchGrossProfitByCondition', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};
	
    return {
		searchBpForSO: searchBpForSO,
		searchItemForSO: searchItemForSO,
		searchItemPrice: searchItemPrice,
		searchBatchNumber: searchBatchNumber,
		searchItemWhsOnHand: searchItemWhsOnHand,
		searchSeries: searchSeries,
		addNewSalesOrder: addNewSalesOrder,
		editSalesOrder: editSalesOrder,
		searchOrder: searchOrder,
		getOrderByEntry: getOrderByEntry,
		searchOrderRelation: searchOrderRelation,
		searchOrderStatus: searchOrderStatus,
		searchApprovalOrder: searchApprovalOrder,
		approveOrder: approveOrder,
		approveOrderList: approveOrderList,
		searchSalesPriceHist: searchSalesPriceHist,
		searchGrossProfitByCondition: searchGrossProfitByCondition
	};
	
});