"use strict";

angular.module('NwsFront').service('reportService', function(httpService, userLocalService, dateTimeUtils) {
    
    var serviceEndPoint = '/report';

	var searchInventoryStatus = function(reqObj) {
    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/getReportInventoryStatus', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var getInventoryStatusDetail = function(itemCode) {

		var reqObj = {};

		reqObj.ItemCode = itemCode;
    	reqObj.Company = userLocalService.getCompany();

		return httpService.commonPost(serviceEndPoint + '/getReportInventoryStatusDocument', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchBatchNumber = function(con) {

		let reqObj = angular.copy(con);

    	reqObj.Company = userLocalService.getCompany();

    	reqObj.docDateFrom = dateTimeUtils.dateToValue(con.docDateFrom);
		reqObj.docDateTo = dateTimeUtils.dateToValue(con.docDateTo);
    	reqObj.expDateFrom = dateTimeUtils.dateToValue(con.expDateFrom);
    	reqObj.expDateTo = dateTimeUtils.dateToValue(con.expDateTo);

    	// if(reqObj.docDateFrom && angular.isDate(reqObj.docDateFrom)) {
    	// 	reqObj.docDateFrom = reqObj.docDateFrom.getTime();
    	// }

    	// if(reqObj.docDateTo && angular.isDate(reqObj.docDateTo)) {
    	// 	reqObj.docDateTo = reqObj.docDateTo.getTime();
    	// }

    	// if(reqObj.expDateFrom && angular.isDate(reqObj.expDateFrom)) {
    	// 	reqObj.expDateFrom = reqObj.expDateFrom.getTime();
    	// }

    	// if(reqObj.expDateTo && angular.isDate(reqObj.expDateTo)) {
    	// 	reqObj.expDateTo = reqObj.expDateTo.getTime();
    	// }

		return httpService.commonPost(serviceEndPoint + '/getReportBathNumberTransaction', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var getBatchNumberDetail = function(req) {

		var reqObj = {};

		reqObj.ItemCode = req.itemCode;
		reqObj.BatchNum = req.batchNumber;
		reqObj.whsCode = req.whsCode;
    	reqObj.Company = userLocalService.getCompany();
    	reqObj.cardCodeFrom = req.cardCodeFrom;
    	reqObj.cardCodeTo = req.cardCodeTo;

		return httpService.commonPost(serviceEndPoint + '/getReportBathNumberTransactionDetail', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchAging = function(conn) {

    	let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();

    	if(reqObj.docDateFrom && angular.isDate(reqObj.docDateFrom)) {
    		reqObj.docDateFrom = reqObj.docDateFrom.getTime();
    	}

    	if(reqObj.docDateTo && angular.isDate(reqObj.docDateTo)) {
    		reqObj.docDateTo = reqObj.docDateTo.getTime();
    	}

    	if(reqObj.docDueDateFrom && angular.isDate(reqObj.docDueDateFrom)) {
    		reqObj.docDueDateFrom = reqObj.docDueDateFrom.getTime();
    	}

    	if(reqObj.docDueDateTo && angular.isDate(reqObj.docDueDateTo)) {
    		reqObj.docDueDateTo = reqObj.docDueDateTo.getTime();
    	}

		return httpService.commonPost(serviceEndPoint + '/getReportCustomerPayment', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchPurchaseMovement = function(conn) {

    	let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();

    	if(reqObj.etaDateFrom && angular.isDate(reqObj.etaDateFrom)) {
    		reqObj.etaDateFrom = reqObj.etaDateFrom.getTime();
    	}

    	if(reqObj.etaDateTo && angular.isDate(reqObj.etaDateTo)) {
    		reqObj.etaDateTo = reqObj.etaDateTo.getTime();
    	}

		return httpService.commonPost(serviceEndPoint + '/getReportPurchaseOrderMovement', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchPurchaseMovementCost = function(conn) {

    	let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();

    	if(reqObj.etaDateFrom && angular.isDate(reqObj.etaDateFrom)) {
    		reqObj.etaDateFrom = reqObj.etaDateFrom.getTime();
    	}

    	if(reqObj.etaDateTo && angular.isDate(reqObj.etaDateTo)) {
    		reqObj.etaDateTo = reqObj.etaDateTo.getTime();
    	}

		return httpService.commonPost(serviceEndPoint + '/getReportPurchaseOrderMovementCost', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	var searchSalesAnalysis = function(conn) {

    	let reqObj = angular.copy(conn);
    	
    	reqObj.Company = userLocalService.getCompany();

    	if(reqObj.docDateFrom && angular.isDate(reqObj.docDateFrom)) {
    		reqObj.docDateFrom = reqObj.docDateFrom.getTime();
    	}

    	if(reqObj.docDateTo && angular.isDate(reqObj.docDateTo)) {
    		reqObj.docDateTo = reqObj.docDateTo.getTime();
    	}

		return httpService.commonPost(serviceEndPoint + '/getReportSalesAnalysis', {'Condition': reqObj}).then(function(response) {
			if(response.response && response.response.length===0){
				response.response = null;
			}
			return response;
		});
	};

	//reponse file
	var getOnHand = function(conn) {

		let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();
    	reqObj.exportType = '5';
    	reqObj.reportName = "WGE-OnHandReport-NoCost";

		return httpService.commonPost(serviceEndPoint + '/getReportOnHand', {'Condition': reqObj}).then(function(response) {
			return response.fileName;
		});
	};

	var getOnHandCost = function(conn) {

		let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();
    	reqObj.exportType = '5';
    	reqObj.reportName = "WGE-OnHandReport-Cost";

		return httpService.commonPost(serviceEndPoint + '/getReportOnHandCost', {'Condition': reqObj}).then(function(response) {
			return response.fileName;
		});
	};

	var getSalesBackOrder = function(conn) {

		let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();
    	reqObj.exportType = '5';
    	reqObj.reportName = "WGE-SalesBackOrderReport";

    	if(reqObj.docDueDateFrom && angular.isDate(reqObj.docDueDateFrom)) {
    		reqObj.docDueDateFrom = reqObj.docDueDateFrom.getTime();
    	}

    	if(reqObj.docDueDateTo && angular.isDate(reqObj.docDueDateTo)) {
    		reqObj.docDueDateTo = reqObj.docDueDateTo.getTime();
    	}

		return httpService.commonPost(serviceEndPoint + '/getReportSalesBackorder', {'Condition': reqObj}).then(function(response) {
			return response.fileName;
		});
	};

	var getReportSalesOrder = function(conn) {
		let reqObj = angular.copy(conn);

    	reqObj.Company = userLocalService.getCompany();
    	// reqObj.exportType = '5';
    	reqObj.reportName = "WGE-SalesOrderReport";

    	return httpService.commonPost(serviceEndPoint + '/getReportSalesOrder', {'Condition': reqObj}).then(function(response) {
			return response.fileName;
		});
	};

    return {
		searchInventoryStatus: searchInventoryStatus,
		getInventoryStatusDetail: getInventoryStatusDetail,
		searchBatchNumber: searchBatchNumber,
		getBatchNumberDetail: getBatchNumberDetail,
		searchAging: searchAging,
		searchPurchaseMovement: searchPurchaseMovement,
		searchPurchaseMovementCost:searchPurchaseMovementCost,
		searchSalesAnalysis: searchSalesAnalysis,
		getOnHand: getOnHand,
		getOnHandCost: getOnHandCost,
		getSalesBackOrder: getSalesBackOrder,
		getReportSalesOrder: getReportSalesOrder
	};
	
});