"use strict";

angular.module('NwsFront').service('constantService', function($q, httpService, appConfig, userLocalService, storageService, localStorageConfig, $translate) {
    
    var serviceEndPoint = '/common';

    var getPublicConstants = function() {
    	let deferred = $q.defer();
	
    	let reqObj = {Company: appConfig.Company};
    	let httpPromise = httpService.asyncPost(serviceEndPoint + '/getConstant', reqObj);
    	httpPromise.then(function(response) {
  	
    		switch (response.status) {
    			case 'SUCCESS':
	            	  
					storageService.setData(localStorageConfig.companyList, response.company);
					
					deferred.resolve(response);
					break;
    			default:
    				deferred.reject(response);
    			break;
    		}
      	
    	}, function(error) {
          deferred.reject(error);
    	});

    	return deferred.promise;
    };

    var getAllConstants = function() {
    	let deferred = $q.defer();

    	let docStatus = getOrderStatus();

    	if(docStatus){
    		deferred.resolve('Skip by already');
    	}else{

	    	let comp = userLocalService.getCompany();
		
			if(comp){
		    	let reqObj = {Company:comp};
		    	let httpPromise = httpService.asyncPost(serviceEndPoint + '/getConstantCompany', reqObj);
		    	httpPromise.then(function(response) {
		  	
		    		switch (response.status) {
		    			case 'SUCCESS':
			            	  
							storageService.setData(localStorageConfig.salesEmployee, response.salesEmployee);
							storageService.setData(localStorageConfig.bpGroup, response.bpGroup);
							storageService.setData(localStorageConfig.paymentTerms, response.paymentTerms);
							storageService.setData(localStorageConfig.shippingType, response.shippingType);
							storageService.setData(localStorageConfig.addressType, response.addressType);
							storageService.setData(localStorageConfig.itemGroup, response.itemGroup);
							storageService.setData(localStorageConfig.whse, response.whse);
							storageService.setData(localStorageConfig.properties, response.properties);
							storageService.setData(localStorageConfig.country, response.country);
							storageService.setData(localStorageConfig.taxGroup, response.taxGroup);
							storageService.setData(localStorageConfig.state, response.state);
							storageService.setData(localStorageConfig.dimension1, response.dimension1);
							storageService.setData(localStorageConfig.dimension2, response.dimension2);
							storageService.setData(localStorageConfig.dimension3, response.dimension3);
							storageService.setData(localStorageConfig.dimension4, response.dimension4);
							storageService.setData(localStorageConfig.dimension5, response.dimension5);
							storageService.setData(localStorageConfig.series, response.series);
							storageService.setData(localStorageConfig.priceList, response.priceList);
							storageService.setData(localStorageConfig.orderStatus, response.documentStatus);
							storageService.setData(localStorageConfig.brand, response.brand);
							storageService.setData(localStorageConfig.channel, response.channel);
							storageService.setData(localStorageConfig.approveStatus, response.approveStatus);
							
							deferred.resolve(response);
							break;
		    			default:
		    				deferred.reject(response);
		    			break;
		    		}
		      	
		    	}, function(error) {
		          deferred.reject(error);
		    	});
		    }else{
		    	deferred.reject('null company');
		    }
		}

    	return deferred.promise;
	};

	var getConstantCompanyBigData = function() {
    	let deferred = $q.defer();

    	let docStatus = getOrderStatus();

    	if(docStatus){
    		deferred.resolve('Skip by already');
    	}else{

	    	let comp = userLocalService.getCompany();
		
			if(comp){
		    	let reqObj = {Company:comp};
		    	let httpPromise = httpService.asyncPost(serviceEndPoint + '/getConstantCompanyBigData', reqObj);
		    	httpPromise.then(function(response) {
		  	
		    		switch (response.status) {
		    			case 'SUCCESS':

							storageService.setData(localStorageConfig.locationAddress, response.locationAddress);
							storageService.setData(localStorageConfig.uomGroup, response.uomGroupConverter);
							
							deferred.resolve(response);
							break;
		    			default:
		    				deferred.reject(response);
		    			break;
		    		}
		      	
		    	}, function(error) {
		          deferred.reject(error);
		    	});
		    }else{
		    	deferred.reject('null company');
		    }
		}

    	return deferred.promise;
	};

	var getConstantLabelByValue = function(constantList, value){
      
		if(!constantList){
			return;
		}

		for(let i=0;i<constantList.length;i++){
			let constant = constantList[i];

			if(constant.value===value){
				return constant;
			}
		}
	};

	var getCompanyList = function() {
		return storageService.getData(localStorageConfig.companyList);
	};

	var getSalesEmployee = function() {
		return storageService.getData(localStorageConfig.salesEmployee);
	};

	var getBpGroup = function() {
		return storageService.getData(localStorageConfig.bpGroup);
	};

	var getPaymentTerms = function() {
		return storageService.getData(localStorageConfig.paymentTerms);
	};
  
	var getShippingType = function() {
		return storageService.getData(localStorageConfig.shippingType);
	};

	var getAddressType = function() {
		return storageService.getData(localStorageConfig.addressType);
	};
	
	var getItemGroup = function() {
		return storageService.getData(localStorageConfig.itemGroup);
	};

	var getWhse = function() {
		return storageService.getData(localStorageConfig.whse);
	};

	var getProperties = function() {
		return storageService.getData(localStorageConfig.properties);
	};

	var getCountry = function() {
		return storageService.getData(localStorageConfig.country);
	};

	var getTaxGroup = function() {
		return storageService.getData(localStorageConfig.taxGroup);
	};

	var getState = function() {
		return storageService.getData(localStorageConfig.state);
	};

	var getDimension1 = function() {
		let constants = storageService.getData(localStorageConfig.dimension1);

		if(constants && constants.length>0){
			return constants;
		}

		return null;
	};

	var getDimension2 = function() {
		let constants = storageService.getData(localStorageConfig.dimension2);

		if(constants && constants.length>0){
			return constants;
		}

		return null;
	};

	var getDimension3 = function() {
		let constants = storageService.getData(localStorageConfig.dimension3);

		if(constants && constants.length>0){
			return constants;
		}

		return null;
	};

	var getDimension4 = function() {
		let constants = storageService.getData(localStorageConfig.dimension4);

		if(constants && constants.length>0){
			return constants;
		}

		return null;
	};

	var getDimension5 = function() {
		let constants = storageService.getData(localStorageConfig.dimension5);

		if(constants && constants.length>0){
			return constants;
		}

		return null;
	};

	var getSeries = function() {
		return storageService.getData(localStorageConfig.series);
	};

	var getPriceList = function() {
		return storageService.getData(localStorageConfig.priceList);
	};

	var getOrderStatus = function() {
		return storageService.getData(localStorageConfig.orderStatus);
	};

	var getBrand = function() {
		return storageService.getData(localStorageConfig.brand);
	};

	var getChannel = function() {
		return storageService.getData(localStorageConfig.channel);
	};

	var getLocationAddress = function() {
		return storageService.getData(localStorageConfig.locationAddress);
	};

	var getUomGroup = function() {
		return storageService.getData(localStorageConfig.uomGroup);
	};

	var getApproveStatus = function() {
		return storageService.getData(localStorageConfig.approveStatus);
	};

	return {
		getPublicConstants: getPublicConstants,
		getAllConstants: getAllConstants,
		getConstantCompanyBigData: getConstantCompanyBigData,
		getConstantLabelByValue: getConstantLabelByValue,
		getSalesEmployee: getSalesEmployee,
		getBpGroup: getBpGroup,
		getPaymentTerms: getPaymentTerms,
		getShippingType: getShippingType,
		getAddressType: getAddressType,
		getItemGroup: getItemGroup,
		getWhse: getWhse,
		getProperties: getProperties,
		getCountry: getCountry,
		getTaxGroup: getTaxGroup,
		getState: getState,
		getDimension1: getDimension1,
		getDimension2: getDimension2,
		getDimension3: getDimension3,
		getDimension4: getDimension4,
		getDimension5: getDimension5,
		getSeries: getSeries,
		getPriceList: getPriceList,
		getOrderStatus: getOrderStatus,
		getBrand: getBrand,
		getChannel: getChannel,
		getLocationAddress: getLocationAddress,
		getUomGroup: getUomGroup,
		getCompanyList: getCompanyList,
		getApproveStatus: getApproveStatus
	};
	
});