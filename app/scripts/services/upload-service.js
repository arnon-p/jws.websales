'use strict';

angular.module('NwsFront').factory('uploadService', function(httpService, userLocalService, appEndpoint, $q, storageService, localStorageConfig) {

    //declare variable
    var uploadServiceEndPoint = '/common';
    var downloadServiceEndPoint = appEndpoint.baseUrl + '/common';

    // var uploadProcessFile = function(reqObj, pathObj) {
    //     return uploadFile('/uploadFile/' + pathObj.imt, reqObj);
    // };

    // var uploadOrderRefFile = function(reqObj, pathObj) {
    //     return uploadFile('/uploadOrderRef', reqObj);
    // };    

    //declare function
    var uploadFile = function(reqObj) {

        return httpService.asyncPostMultipart(uploadServiceEndPoint + '/upload', reqObj).then(function(result){
        	storageService.setData(localStorageConfig.imageVersion, new Date().getTime());
        	return result;
        });
    };

    var getImageVersion = function() {
        let v = storageService.getData(localStorageConfig.imageVersion);

        if (!v) {
            v = new Date().getTime();

            setImageVersion(v);
        }

        return v;
    };

    var setImageVersion = function(v) {
        storageService.setData(localStorageConfig.imageVersion, v);
    };

    var getDownloadFileLink = function(fileName){
        return downloadServiceEndPoint + '/getAttachmentFiles/' + userLocalService.getCompany() + '/' + fileName;
    };

    var getDownloadTempFileLink = function(fileName){
        return downloadServiceEndPoint + '/getTempFiles/' + fileName;
    };

    var getBrochureList = function() {
        let reqObj = {};

        reqObj.Company = userLocalService.getCompany();

        return httpService.commonPost(uploadServiceEndPoint + '/getBrochure', {'condition': reqObj}).then(function(response) {
            if(response.response){
                if(response.response.length===0){
                    response.response = null;
                // }else{
                //  let resp = [];

                //  for(let i=0;i<response.response.length;i++){
                //      let filename = response.response[i];

                //      let obj = {
                //          fileName: filename
                //      };

                //      resp.push(obj);
                //  }

                //  response.response = resp;
                }
            }
            return response;
        });
    };

    var uploadBrochure = function(reqObj) {

        return httpService.asyncPostMultipart(uploadServiceEndPoint + '/uploadBrochure', reqObj).then(function(result){
            storageService.setData(localStorageConfig.imageVersion, new Date().getTime());
            return result;
        });
    };

    var getDownloadBrochureLink = function(fileName){
        return downloadServiceEndPoint + '/getBrochureFiles/' + userLocalService.getCompany() + '/' + fileName;
    };

    var deleteBrochureByName = function(fileName){
        let reqObj = {FileName: fileName};

        reqObj.Company = userLocalService.getCompany();

        return httpService.commonPost(uploadServiceEndPoint + '/deleteBrochure', reqObj).then(function(response) {
            return response;
        });
    };

    return {
    	// uploadProcessFile: uploadProcessFile,
     //    uploadOrderRefFile: uploadOrderRefFile,
        uploadFile: uploadFile,
        uploadBrochure: uploadBrochure,
        getImageVersion: getImageVersion,
        setImageVersion: setImageVersion,
        getDownloadFileLink: getDownloadFileLink,
        getDownloadTempFileLink: getDownloadTempFileLink,
        getDownloadBrochureLink: getDownloadBrochureLink,
        getBrochureList: getBrochureList,
        deleteBrochureByName: deleteBrochureByName
    };
});
