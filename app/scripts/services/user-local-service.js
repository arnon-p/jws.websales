"use strict";

angular.module('NwsFront').service('userLocalService', function($q, storageService, localStorageConfig, $base64) {

	var clearUserData = function() {
        $q(function(resolve, reject){
            setUserObj(null);
            setToken(null);
        });
    };
    var getUserObj = function() {
        return storageService.getData(localStorageConfig.userProfile);
    };
    var setUserObj = function(userObj) {

        if(userObj){
            //set display name
            let display = 'Welcome ';

            if(userObj.profileBean.position.positionName){
                display = display + userObj.profileBean.position.positionName + ' ';
            }

            if(userObj.profileBean.firstName){
                display = display + userObj.profileBean.firstName + ' ';
            }

            if(userObj.profileBean.lastName){
                display = display + userObj.profileBean.lastName + ' ';
            }

            userObj.display = display;
        }

        return storageService.setData(localStorageConfig.userProfile, userObj);
    };
    var getUserLevel = function() {
        var userObj = storageService.getData(localStorageConfig.userProfile);
        
        if (userObj && userObj.userLevel) {
            return userObj.userLevel;
        }

        return null;
    };
    var getToken = function() {
        var token = storageService.getData(localStorageConfig.userToken);

        if (!token) {
            token = 'd2lraTpwZWRpY==='; //simple token
        } else {
            token = $base64.decode(token);
        }

        return token;
    };
    var setToken = function(token) {
        if (token) {
            token = $base64.encode(token);
        }

        storageService.setData(localStorageConfig.userToken, token);
    };
    var getCompany = function(){
        return storageService.getData(localStorageConfig.userCompany);
    };
    var setCompany = function(comp){
        storageService.setData(localStorageConfig.userCompany, comp);
    };
    var isLoggedIn = function() {
        return getUserObj() != null;
    };
    
    return {
    	clearUserData: clearUserData,
    	getUserObj: getUserObj,
    	setUserObj: setUserObj,
    	getUserLevel: getUserLevel,
    	getToken: getToken,
    	setToken: setToken,
    	isLoggedIn: isLoggedIn,
        getCompany: getCompany,
        setCompany: setCompany
    };
});