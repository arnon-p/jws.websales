"use strict";

angular.module('NwsFront').controller('orderStatusViewCtrl', function ($scope, $q, loaderService, exceptionService, documentService, storageService, localStorageConfig){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.localCondition = null;
    // $scope.paginable = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = storageService.getData(localStorageConfig.editBean);

        $scope.doSearch();
    };

    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        documentService.searchOrderStatus($scope.condition).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            // $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();

        loaderService.hide();
    });
});