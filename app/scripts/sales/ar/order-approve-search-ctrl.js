"use strict";

angular.module('NwsFront').controller('orderApproveSearchCtrl', function ($scope, masterService, constantService, localStorageConfig, dialogService, storageService, exceptionService, loaderService, $q, Pagination, documentService){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.paginable = null;

    $scope.statusList = null;
    $scope.salesEmployeeList = null;
    $scope.approveStatusList = null;
    $scope.remark = null;

    $scope.salesExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

    //declare function
    $scope.initForm = function() {
        $scope.condition = {wddStatus: '-', selectedSales: []};
        $scope.resultList = null;
        $scope.paginable = Pagination.getDefaultPaginable();
        $scope.remark = null;

        //constant
        if(!$scope.statusList){
            $scope.statusList = constantService.getOrderStatus();
        }

        if(!$scope.approveStatusList){
            $scope.approveStatusList = constantService.getApproveStatus();
        }

        if(!$scope.salesEmployeeList){
            let salesEmployees = constantService.getSalesEmployee();

            $scope.salesEmployeeList = [];

            for(let i=0;i<salesEmployees.length;i++){
                let sales = salesEmployees[i];

                let sale = {};

                sale.id = sales.slpCode;
                sale.label = sales.slpName;
                sale.value = i;
                sale.name = sales.slpName;

                $scope.salesEmployeeList.push(sale);
            }
        }
    };
    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        let cond = angular.copy($scope.condition);

        delete cond['selectedSales'];

        cond.slpCode = $scope.getSelectedSales();

        documentService.searchApprovalOrder(cond).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };
    $scope.getSelectedSales = function() {
        if($scope.condition.selectedSales && $scope.condition.selectedSales.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedSales, function(sale){
                result.push(sale.id);
            });

            return result;
        }else{
            return null;
        }
    };
    $scope.gotoDetail = function(e, row) {
        loaderService.show(true);

        if(row){
            let data = angular.copy(row);

            data.docDate = null;
            data.docDueDate = null;
            data.taxDate = null;

            storageService.setData(localStorageConfig.editBean, data);
        }else{
            storageService.setData(localStorageConfig.editBean, null);
        }
        
        $scope.gotoState('app.sales.arorderapproveedit');
    };
    $scope.getOrderStatusName = function(statusCode){
        return constantService.getConstantLabelByValue($scope.statusList, statusCode);
    };
    $scope.getSalesEmployeeName = function(slpCode){
        let slpName;

        for(let i=0;i<$scope.salesEmployeeList.length;i++){
            let constant = $scope.salesEmployeeList[i];

            if(constant.id===slpCode){
                slpName = constant.name;

                break;
            }
        }

        return slpName;
    };
    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        },
        itemSelected: function (e) {
            $scope.condition.cardCode = e.item.cardCode;
            $scope.condition.cardName = e.item.cardName;
        }
    };
    $scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.getAllChecked = function() {
        if ($scope.resultList) {
            let checked = [];
            for (let i = 0; i < $scope.resultList.length; i++) {
                let row = $scope.resultList[i];
                if (row.checked) {
                    checked.push(row);
                }
            }
            return checked;
        }
        return null;
    };
    $scope.toggleObjSelection = function($event, description) {
        $event.stopPropagation();
    };
    $scope.markAllResultIsClicked = function(e) {
        angular.forEach($scope.resultList, function(itm) {
            itm.checked = event.target.checked;
        });
    };
    $scope.doApprove = function(e) {
        let deferred = $q.defer();

        let selected = $scope.getAllChecked();

        if(!selected || selected.length===0){
            dialogService.warningSelectRecord($scope);

            deferred.reject(error);
        }else{

            loaderService.show();

            for(let i=0;i<selected.length;i++){
                selected[i].wddStatus = 'Y';
                selected[i].remark = $scope.remark;
            }

            let savePromise = documentService.approveOrderList(selected);

            savePromise.then(function(res) {
                deferred.resolve(res);
                // $scope.bean = res;

                // storageService.setData(localStorageConfig.editBean, res);

                loaderService.hide();

                dialogService.saveSuccessWithFunction($scope, function() {
                    // $scope.initForm();
                    // $scope.gotoState('app.sales.arorderapprovesearch');
                    $scope.remark = null;
                    $scope.doSearch(e);
                });
            }, function(error) {
                loaderService.hide();
                deferred.reject(error);
                exceptionService.handler($scope, error);
            });
        }

        return deferred.promise;
    };
    $scope.doReject = function(e) {
        let deferred = $q.defer();

        let selected = $scope.getAllChecked();

        if(!selected || selected.length===0){
            dialogService.warningSelectRecord($scope);

            deferred.reject(error);
        }else{

            loaderService.show();

            for(let i=0;i<selected.length;i++){
                selected[i].wddStatus = 'N';
                selected[i].remark = $scope.remark;
            }

            let savePromise = documentService.approveOrderList(selected);

            savePromise.then(function(res) {
                deferred.resolve(res);
                // $scope.bean = res;

                // storageService.setData(localStorageConfig.editBean, res);

                loaderService.hide();

                dialogService.saveSuccessWithFunction($scope, function() {
                    // $scope.initForm();
                    // $scope.gotoState('app.sales.arorderapprovesearch');
                    $scope.remark = null;
                    $scope.doSearch(e);
                });
            }, function(error) {
                loaderService.hide();
                deferred.reject(error);
                exceptionService.handler($scope, error);
            });
        }

        return deferred.promise;
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});