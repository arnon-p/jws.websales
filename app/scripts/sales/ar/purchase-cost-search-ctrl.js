"use strict";

angular.module('NwsFront').controller('purchaseReportCostSearchCtrl', function ($scope, masterService, constantService, localStorageConfig, storageService, loaderService){
    //declare variable
    $scope.condition = null;
    $scope.statusList = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = {};

        //constant
        if(!$scope.statusList){
            $scope.statusList = constantService.getOrderStatus();
        }
    };
    $scope.gotoDetail = function(e) {
        loaderService.show(true);

        if($scope.condition.etaDateFrom && angular.isDate($scope.condition.etaDateFrom)) {
            $scope.condition.etaDateFrom = $scope.condition.etaDateFrom.getTime();
        }

        if($scope.condition.etaDateTo && angular.isDate($scope.condition.etaDateTo)) {
            $scope.condition.etaDateTo = $scope.condition.etaDateTo.getTime();
        }
        
        storageService.setData(localStorageConfig.editBean, $scope.condition);
        $scope.gotoState('app.sales.arpurchasecostview');
    };
    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText, 'S').then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});