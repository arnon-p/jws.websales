"use strict";

angular.module('NwsFront').controller('orderStatusSearchCtrl', function ($scope, masterService, constantService, localStorageConfig, storageService, loaderService){
	//declare variable
    $scope.condition = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = {};
    };
    $scope.gotoDetail = function(e) {
        loaderService.show(true);

        if($scope.condition.docDueDateFrom && angular.isDate($scope.condition.docDueDateFrom)) {
            $scope.condition.docDueDateFrom = $scope.condition.docDueDateFrom.getTime();
        }

        if($scope.condition.docDueDateTo && angular.isDate($scope.condition.docDueDateTo)) {
            $scope.condition.docDueDateTo = $scope.condition.docDueDateTo.getTime();
        }
        
        storageService.setData(localStorageConfig.editBean, $scope.condition);
        $scope.gotoState('app.sales.arorderstatusview');
    };
    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});