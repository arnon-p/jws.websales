"use strict";

angular.module('NwsFront').controller('analysisReportSearchCtrl', function ($scope, dialogService, localStorageConfig, storageService, constantService, masterService, loaderService){
	//declare variable
	$scope.condition = null;

	$scope.salesEmployeeList = null;

    $scope.salesExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

	$scope.initForm = function() {
		$scope.condition = {
			reportType: 'A',
			dataType: 'A',
			displayType: 'A',
			totalType: 'A',
            selectedSales: []
		};

		//constant
		if(!$scope.salesEmployeeList){
            let salesEmployees = constantService.getSalesEmployee();

            $scope.salesEmployeeList = [];

            for(let i=0;i<salesEmployees.length;i++){
                let sales = salesEmployees[i];

                let sale = {};

                sale.id = sales.slpCode;
                sale.label = sales.slpName;
                sale.value = i;
                sale.name = sales.slpName;

                $scope.salesEmployeeList.push(sale);
            }
        }
	};
	$scope.gotoDetail = function(e) {
        
        //validate
        {
            if(!$scope.condition.selectedSales || $scope.condition.selectedSales.length===0){
                dialogService.showAlertDialog($scope, 'Warning', 'Please select at least one sales employee.');

                return false;
            }
            
        }

        loaderService.show(true);

        let cond = angular.copy($scope.condition);

        delete cond['selectedSales'];

        cond.slpCode = $scope.getSelectedSales();

        if($scope.condition.docDateFrom && angular.isDate($scope.condition.docDateFrom)) {
            cond.docDateFrom = $scope.condition.docDateFrom.getTime();
        }

        if($scope.condition.docDateTo && angular.isDate($scope.condition.docDateTo)) {
            cond.docDateTo = $scope.condition.docDateTo.getTime();
        }
        
        storageService.setData(localStorageConfig.editBean, cond);
        $scope.gotoState('app.sales.aranalysisreportedit');
    };

    $scope.validateForm = function(e){
        //Please select at least one record to delete.

        let valid = false;

        valid = $scope.condition.selectedSales && $scope.condition.selectedSales.length>0;

        return valid;
    };

	$scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.getSelectedSales = function() {
        if($scope.condition.selectedSales && $scope.condition.selectedSales.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedSales, function(sale){
                result.push(sale.id);
            });

            return result;
        }else{
            return null;
        }
    };

	//event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});