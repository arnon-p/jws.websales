"use strict";

angular.module('NwsFront').controller('orderSearchCtrl', function ($scope, masterService, constantService, localStorageConfig, storageService, exceptionService, loaderService, $q, Pagination, documentService){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.paginable = null;

    $scope.statusList = null;
    $scope.salesEmployeeList = null;

    $scope.salesExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

    //declare function
    $scope.initForm = function() {
        $scope.condition = {selectedSales: []};
        $scope.resultList = null;
        $scope.paginable = Pagination.getDefaultPaginable();

        //constant
        if(!$scope.statusList){
            $scope.statusList = constantService.getOrderStatus();
        }

        if(!$scope.salesEmployeeList){
            let salesEmployees = constantService.getSalesEmployee();

            $scope.salesEmployeeList = [];

            for(let i=0;i<salesEmployees.length;i++){
                let sales = salesEmployees[i];

                let sale = {};

                sale.id = sales.slpCode;
                sale.label = sales.slpName;
                sale.value = i;
                sale.name = sales.slpName;

                $scope.salesEmployeeList.push(sale);
            }
        }
    };
    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        let cond = angular.copy($scope.condition);

        delete cond['selectedSales'];

        cond.slpCode = $scope.getSelectedSales();

        documentService.searchOrder(cond).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };
    $scope.getSelectedSales = function() {
        if($scope.condition.selectedSales && $scope.condition.selectedSales.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedSales, function(sale){
                result.push(sale.id);
            });

            return result;
        }else{
            return null;
        }
    };
    $scope.gotoDetail = function(e, row) {
        loaderService.show(true);

        if(row){
            let data = angular.copy(row);

            data.docDate = null;
            data.docDueDate = null;
            data.taxDate = null;

            storageService.setData(localStorageConfig.editBean, data);
        }else{
            storageService.setData(localStorageConfig.editBean, null);
        }
        
        $scope.gotoState('app.sales.arorderedit');
    };
    $scope.getOrderStatusName = function(statusCode){
        return constantService.getConstantLabelByValue($scope.statusList, statusCode);
    };
    $scope.getSalesEmployeeName = function(slpCode){
        let slpName;

        for(let i=0;i<$scope.salesEmployeeList.length;i++){
            let constant = $scope.salesEmployeeList[i];

            if(constant.id===slpCode){
                slpName = constant.name;

                break;
            }
        }

        return slpName;
    };
    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        },
        itemSelected: function (e) {
            $scope.condition.cardCode = e.item.cardCode;
            $scope.condition.cardName = e.item.cardName;
        }
    };
    $scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});