"use strict";

angular.module('NwsFront').controller('onHandSearchCtrl', function ($scope, masterService, exceptionService, loaderService, $q, constantService, reportService){
	
	//declare variable
	$scope.condition = null;
	$scope.whseList = null;
	$scope.brandList = null;
	$scope.channelList = null;

    $scope.multiSelectExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

	//declare function
	$scope.initForm = function() {
		//constant
		if(!$scope.whseList){
            let whseCodeList = constantService.getWhse();

            $scope.whseList = [];

            for(var i=0;i<whseCodeList.length;i++){
                let whseCode = whseCodeList[i];

                let whse = {};

                whse.id = whseCode;
                whse.label = whseCode;
                whse.value = i;

                $scope.whseList.push(whse);
            }
        }

        if(!$scope.brandList){
            let brandList = constantService.getBrand();

            $scope.brandList = [];

            for(var i=0;i<brandList.length;i++){
                let brand = brandList[i];

                let b = {};

                b.id = brand.code;
                b.label = brand.name;
                b.value = i;

                $scope.brandList.push(b);
            }
        }

        if(!$scope.channelList){
            let channelList = constantService.getChannel();

            $scope.channelList = [];

            for(var i=0;i<channelList.length;i++){
                let channel = channelList[i];

                let b = {};

                b.id = channel.code;
                b.label = channel.name;
                b.value = i;

                $scope.channelList.push(b);
            }
        }

        $scope.condition = {
            selectedWhse: [],
            selectedBrand: [],
            selectedChannel: []
        };
	};

	$scope.getSelectedWhse = function() {
        if($scope.condition.selectedWhse && $scope.condition.selectedWhse.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedWhse, function(whse){
                result.push(whse.id);
            });

            return result;
        }else{
            return null;
        }
    };

    $scope.getSelectedBrand = function() {
        if($scope.condition.selectedBrand && $scope.condition.selectedBrand.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedBrand, function(brand){
                result.push(brand.id);
            });

            return result;
        }else{
            return null;
        }
    };

    $scope.getSelectedChannel = function() {
        if($scope.condition.selectedChannel && $scope.condition.selectedChannel.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedChannel, function(channel){
                result.push(channel.id);
            });

            return result;
        }else{
            return null;
        }
    };

	$scope.doExport = function(e) {
		var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        let cond = angular.copy($scope.condition);

        delete cond['selectedWhse'];
        delete cond['selectedBrand'];
        delete cond['selectedChannel'];

        cond.whsCode = $scope.getSelectedWhse();
        cond.brand = $scope.getSelectedBrand();
        cond.channel = $scope.getSelectedChannel();

        reportService.getOnHand(cond).then(function(response) {

            loaderService.hide();

            deferred.resolve(response);

            $scope.downloadReport(response);
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
	};

	$scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };

	//declare event
	$scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});