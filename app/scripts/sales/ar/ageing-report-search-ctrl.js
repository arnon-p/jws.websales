"use strict";

angular.module('NwsFront').controller('ageingReportSearchCtrl', function ($scope, masterService, constantService, localStorageConfig, storageService, loaderService){
	//declare variable
    $scope.condition = null;
    $scope.salesEmployeeList = null;

    $scope.salesExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

    //declare function
    $scope.initForm = function() {
        $scope.condition = {selectedSales: []};

        //constant
        if(!$scope.salesEmployeeList){
            let salesEmployees = constantService.getSalesEmployee();

            $scope.salesEmployeeList = [];

            for(let i=0;i<salesEmployees.length;i++){
                let sales = salesEmployees[i];

                let sale = {};

                sale.id = sales.slpCode;
                sale.label = sales.slpName;
                sale.value = i;
                sale.name = sales.slpName;

                $scope.salesEmployeeList.push(sale);
            }
        }
    };
    $scope.gotoDetail = function(e) {
        loaderService.show(true);

        let cond = angular.copy($scope.condition);

        delete cond['selectedSales'];

        cond.slpCode = $scope.getSelectedSales();

        if($scope.condition.docDateFrom && angular.isDate($scope.condition.docDateFrom)) {
            cond.docDateFrom = $scope.condition.docDateFrom.getTime();
        }

        if($scope.condition.docDateTo && angular.isDate($scope.condition.docDateTo)) {
            cond.docDateTo = $scope.condition.docDateTo.getTime();
        }

        if($scope.condition.docDueDateFrom && angular.isDate($scope.condition.docDueDateFrom)) {
            cond.docDueDateFrom = $scope.condition.docDueDateFrom.getTime();
        }

        if($scope.condition.docDueDateTo && angular.isDate($scope.condition.docDueDateTo)) {
            cond.docDueDateTo = $scope.condition.docDueDateTo.getTime();
        }
        
        storageService.setData(localStorageConfig.editBean, cond);
        $scope.gotoState('app.sales.arageingreportview');
    };
    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.getSelectedSales = function() {
        if($scope.condition.selectedSales && $scope.condition.selectedSales.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedSales, function(sale){
                result.push(sale.id);
            });

            return result;
        }else{
            return null;
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});