"use strict";

angular.module('NwsFront').controller('analysisReportViewCtrl', function ($scope, reportService, $q, exceptionService, loaderService, localStorageConfig, storageService){
	
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.localCondition = null;
    $scope.dynamicMonthTh = null;

	//declare function
    $scope.initForm = function() {
        $scope.condition = storageService.getData(localStorageConfig.editBean);

        $scope.doSearch();
    };

    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        reportService.searchSalesAnalysis($scope.condition).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            // $scope.paginable = response.paginable;

            if($scope.resultList && $scope.resultList.length>0){
            	$scope.dynamicMonthTh = $scope.resultList[0].months;
            }

            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.sumYtdQty = function(resultList){
        let sum = 0.0;

        for(let i=0;i<resultList.length;i++){
            sum = sum + resultList[i].ytdQuantity;
        }

        return sum;
    };

    $scope.sumYtdTotal = function(resultList){
        let sum = 0.0;

        for(let i=0;i<resultList.length;i++){
            sum = sum + resultList[i].ytdTotal;
        }

        return sum;
    };

    $scope.sumMonthQty = function(monthIndex){
        let sum = 0.0;

        for(let i=0;i<$scope.resultList.length;i++){
            let result = $scope.resultList[i];

            if(result.months && result.months.length >= monthIndex){
                let month = result.months[monthIndex];

                sum = sum + month.quantity;
            }
        }

        return sum;
    };

    $scope.sumMonthTotal = function(monthIndex){
        let sum = 0.0;

        for(let i=0;i<$scope.resultList.length;i++){
            let result = $scope.resultList[i];

            if(result.months && result.months.length >= monthIndex){
                let month = result.months[monthIndex];

                sum = sum + month.total;
            }
        }

        return sum;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();

        loaderService.hide();
    });
});