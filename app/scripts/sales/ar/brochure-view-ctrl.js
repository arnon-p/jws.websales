"use strict";

angular.module('NwsFront').controller('brochureViewCtrl', function ($scope, appConfig, dialogService, uploadService, exceptionService, loaderService){
	//declare variable
    $scope.bean = null;
    $scope.showUpload = false;

    //declare function
    $scope.initForm = function() {
        loaderService.show();

        $scope.bean = {};

        uploadService.getBrochureList().then(function(res){
            $scope.bean.attachments = res.response;

            loaderService.hide();
        }, function(err){
            exceptionService.handler($scope, err);
            loaderService.hide();
        });
    };

    $scope.doUpload = function(){
        $scope.showUpload = !$scope.showUpload;
    };
    $scope.onUploadFile = function(file) {
        if(file==null || file.size > (appConfig.uploadMaxSize * 5)){
            dialogService.showAlertDialog($scope, 'Invalid', 'Not allow file size over 5MB.');
        }else{
            loaderService.show();

            let fd = new FormData();

            fd.append('file', file);

            uploadService.uploadBrochure(fd).then(function(res){

                // console.log(res);
                if(!$scope.bean.attachments) {
                    $scope.bean.attachments = [];
                }

                $scope.bean.attachments.push(res);

                loaderService.hide();
            }, function(err){
                exceptionService.handler($scope, err);
                loaderService.hide();
            }).finally(function(){
                $scope.showUpload = false;
            });
        }
    };
    $scope.doDeleteFile = function(e, index, fileName){
        if(fileName){
            loaderService.show();

            uploadService.deleteBrochureByName(fileName).then(function(){
                $scope.initForm();
                loaderService.hide();
            }, function(err){
                exceptionService.handler($scope, err);
                loaderService.hide();
            });
        }
        // if($scope.bean.attachments){
        //     if($scope.bean.attachments.length>index){
        //         $scope.bean.attachments.splice(index, 1);
        //     }
        // }
    };
    $scope.doDownloadFile = function(e, index, fileObj){
        let url = uploadService.getDownloadBrochureLink(fileObj.fileName);

        $scope.onHref(url);
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
        loaderService.hide();
    });
});