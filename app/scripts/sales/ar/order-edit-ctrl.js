"use strict";

angular.module('NwsFront').controller('orderEditCtrl', function ($scope, $q, $filter, appConfig, uploadService, reportService, dialogService, dateTimeUtils, numberUtils, $timeout, documentService, masterService, constantService, exceptionService, loaderService, localStorageConfig, storageService){
	//declare variable
    $scope.mode = null;
    $scope.bean = null;
    $scope.tabIndex = 0;
    $scope.showUdf = true;
    $scope.showUpload = false;

    $scope.statusList = null;
    $scope.salesEmployeeList = null;
    $scope.seriesList = null;
    $scope.paymentTermsList = null;
    $scope.taxGroupList = null;
    $scope.dimension1List = null;
    $scope.dimension2List = null;
    $scope.dimension3List = null;
    $scope.dimension4List = null;
    $scope.dimension5List = null;
    $scope.uomGroupList = null;

    $scope.selectedPartner = null;
    $scope.contactPersonList = null;
    $scope.shipToList = null;
    $scope.billToList = null;
    $scope.relationList = null;
    $scope.approveStatusList = null;

    $scope.selectedItem = null;

    $scope.newLineIndex = 0;

    $scope.minDate = dateTimeUtils.currentDate();

    $scope.statusDocView = false;   //back view ctrl

    //declare function
    $scope.initForm = function() {
        //constant
        if(!$scope.salesEmployeeList){
            $scope.salesEmployeeList = constantService.getSalesEmployee();
        }
        if(!$scope.seriesList){
            $scope.seriesList = constantService.getSeries();
        }
        if(!$scope.paymentTermsList){
            $scope.paymentTermsList = constantService.getPaymentTerms();
        }
        if(!$scope.taxGroupList){
            $scope.taxGroupList = constantService.getTaxGroup();
        }
        if(!$scope.dimension1List){
            $scope.dimension1List = constantService.getDimension1();
        }
        if(!$scope.dimension2List){
            $scope.dimension2List = constantService.getDimension2();
        }
        if(!$scope.dimension3List){
            $scope.dimension3List = constantService.getDimension3();
        }
        if(!$scope.dimension4List){
            $scope.dimension4List = constantService.getDimension4();
        }
        if(!$scope.dimension5List){
            $scope.dimension5List = constantService.getDimension5();
        }
        if(!$scope.statusList){
            $scope.statusList = constantService.getOrderStatus();
        }
        if(!$scope.uomGroupList){
            $scope.uomGroupList = constantService.getUomGroup();
        }
        if(!$scope.approveStatusList){
            $scope.approveStatusList = constantService.getApproveStatus();
        }

        $scope.bean = storageService.getData(localStorageConfig.editBean);

        if (!$scope.bean) { //add
            $scope.bean = {
                discountPercent: 0,
                status: 'O',
                statusDisp: 'Open'
            };

            $scope.bean.docDate = dateTimeUtils.currentDate();
            $scope.bean.taxDate = dateTimeUtils.currentDate();
            $scope.bean.docDueDate = dateTimeUtils.addDay($scope.bean.docDate, 1);

            $scope.refreshSeries();

            $scope.mode = 'add';
        } else {
            loaderService.show();

            $scope.statusDocView = $scope.bean.statusDocView;   //back view ctrl

            documentService.getOrderByEntry($scope.bean.docEntry, $scope.bean.code, $scope.bean.isDraft).then(function(response) {
                $scope.bean = response;

                loaderService.hide();

                //chain fetch relation data
                // if(!$scope.bean.isDraft){
                    // let relationReqObj = {};

                    // relationReqObj.docEntry = $scope.bean.docEntry;
                    // // relationReqObj.draftKey = $scope.bean.draftKey;

                    // documentService.searchOrderRelation(relationReqObj).then(function(relationRes){
                    //     $scope.relationList = relationRes.response;
                    // });
                    // $scope.reloadDocRelation();
                // }
                $scope.selectedPartner = {
                    cardCode: $scope.bean.cardCode,
                    cardName: $scope.bean.cardName
                };

                //docStatus
                let status = constantService.getConstantLabelByValue($scope.statusList, $scope.bean.docStatus);
                let statusLabel = status ? status.name : '';

                if($scope.bean.isDraft){

                    let wddName = $scope.getApproveStatusName($scope.bean.wddStatus);

                    if(wddName){
                        $scope.bean.statusDisp = statusLabel + ' (' + wddName + ')';
                    }else{
                        $scope.bean.statusDisp = statusLabel;
                    }
                    
                }else{
                    if('Y'===$scope.bean.canceled){
                        $scope.bean.statusDisp = statusLabel + ' (Cancellation)';
                    }else{
                        $scope.bean.statusDisp = statusLabel;
                    }
                }

                //set combo
                $scope.shipToList = response.bpShipList;
                $scope.billToList = response.bpBillList;
                $scope.contactPersonList = response.bpContactList;

                //set uom for batch dialog
                //calculate uom qty
                if($scope.bean.lines){
                    angular.forEach($scope.bean.lines, function(row){
                        if($scope.uomGroupList){
                            for(let i=0;i<$scope.uomGroupList.length;i++){
                                let uomData = $scope.uomGroupList[i];

                                if(uomData.uomEntry===row.uomEntry){
                                    row.uomBaseQty = uomData.baseQty;
                                    row.uomAltQty = uomData.altQty;

                                    break;
                                }
                            }
                        }
                    });
                }
            }, function(error) {
                $scope.bean = null;
                loaderService.hide();
                exceptionService.queryHandler($scope, error);
            });

            $scope.mode = 'edit';
        }
    };
    $scope.reloadDocRelation = function(e){
        let relationReqObj = {};

        relationReqObj.docEntry = $scope.bean.docEntry;
        // relationReqObj.draftKey = $scope.bean.draftKey;

        documentService.searchOrderRelation(relationReqObj).then(function(relationRes){
            $scope.relationList = relationRes.response;
        });
    };
    $scope.doSave = function(e) {
        var deferred = $q.defer();
        loaderService.show();
        var savePromise = null;
        if ('edit'===$scope.mode) {
            savePromise = documentService.editSalesOrder($scope.bean);
        } else {
            savePromise = documentService.addNewSalesOrder($scope.bean);
        }
        savePromise.then(function(res) {
            deferred.resolve(res);
            // $scope.bean = res;

            storageService.setData(localStorageConfig.editBean, res);

            loaderService.hide();

            dialogService.saveSuccessWithFunction($scope, function() {
                $scope.initForm();
            });
        }, function(error) {
            loaderService.hide();
            deferred.reject(error);
            exceptionService.handler($scope, error);
        });
        return deferred.promise;
    };
    $scope.toggleTab = function (e, index) {
        $scope.tabIndex = index;

        //reload doc relation when show tab
        // if(4===index){
        //     $scope.reloadDocRelation();
        // }
    };
    $scope.toggleUdf = function(e){
        $scope.showUdf = !$scope.showUdf;
    };
    $scope.doOpenPartnerDialog = function(e) {
        // if('add'===$scope.mode){
            dialogService.openDialog('searchPartnerModal', $scope, null, null, function () {
                $timeout(function () {
                    let scope = $scope.getScopeById("searchPartnerModal");

                    scope.init();

                    loaderService.hide();
                }, 100);
            });
        // }
    };
    $scope.dialogChoosePartner = function(data) {
        //set data from dialog
        $scope.selectedPartner = data;

        $scope.bean.cardCode = data.cardCode;
        $scope.bean.cardName = data.cardName;
        $scope.bean.salesPersonCode = data.salesPersonCode;
        $scope.contactPersonList = (data.bpContactList && data.bpContactList.length > 0) ? data.bpContactList : null;
        $scope.bean.contactPersonCode = data.cntctPrsnDef
        // $scope.bean.docDate = dateTimeUtils.currentDate();
        // $scope.bean.taxDate = dateTimeUtils.currentDate();
        // $scope.bean.docDueDate = dateTimeUtils.addDay($scope.bean.docDate, 15);
        $scope.shipToList = (data.bpShipList && data.bpShipList.length > 0) ? data.bpShipList : null;
        $scope.billToList = (data.bpBillList && data.bpBillList.length > 0) ? data.bpBillList : null;
        $scope.bean.shipToCode = data.shipToDef;
        $scope.bean.payToCode = data.billToDef;
        $scope.bean.paymentGroupCode = data.payTermsGrpCode;
        $scope.bean.bpTaxId = data.federalTaxID;
        $scope.bean.bpBrnCode = data.bpBrnCode;
        $scope.bean.vatBranch = data.vatBranch;
        $scope.bean.priceListNum = data.priceListNum;
        $scope.bean.priceListName = data.priceListName; 

        // $scope.refreshSeries();
        $scope.doSelectShipTo();
        $scope.doSelectBillTo();
        // $scope.bean.bpBrnCode = data.bpBrnCode;
    };
    $scope.doOpenItemDialog = function(e, rowIndex) {
        if(!$scope.selectedPartner){
            return;
        }

        dialogService.openDialog('searchItemModal', $scope, {bean: $scope.bean, rowIndex: rowIndex}, null, function () {
            $timeout(function () {
                let scope = $scope.getScopeById("searchItemModal");

                scope.init();

                loaderService.hide();
            }, 100);
        });
    };
    $scope.dialogChooseItem = function(data, rowIndex) {
        //set data from dialog
        $scope.selectedItem = data;

        let row = $scope.bean.lines[rowIndex];

        row.itemCode = data.itemCode;
        row.itemName = data.itemName;
        row.bpItemName = data.bpItemName;
        row.barcode = data.codeBars;
        row.docDueDate = $scope.bean.docDueDate;
        row.quantity = 1;
        row.unitPrice = data.unitPrice;
        row.discountPercent = data.discountPercent;
        row.uomEntry = data.uomEntry;
        row.uomCode = data.uomCode;
        row.uomName = data.uomName;
        row.ugpEntry = data.ugpEntry;
        row.vatGroup = data.taxGroup;
        row.vatRate = data.taxRate;

        row.inStockQty = data.inStockQty;
        row.availableQty = data.availableQty;
                // row.price = res.price;
                // row.specialPrice = res.specialPrice;
        row.isBatch = data.isBatch;
        row.isSpecialPrice = data.isSpecialPrice;
        row.barList = data.barList;
        row.uomConvertList = data.uomConvertList;
        row.warehouseCode = data.whsCode;
        row.costingCode = data.costingCode;
        row.priceAfterDiscount = data.price;

        //calculate uom qty
        if($scope.uomGroupList){
            for(let i=0;i<$scope.uomGroupList.length;i++){
                let uomData = $scope.uomGroupList[i];

                if(uomData.ugpEntry === row.ugpEntry && uomData.uomEntry===row.uomEntry){
                    row.uomBaseQty = uomData.baseQty;
                    row.uomAltQty = uomData.altQty;

                    break;
                }
            }
        }

        //calculate not same other event
        // $scope.doCalculateLineAfterChooseItem(null, row);

        $scope.doCalculateLine(null, row);
    };
    $scope.doOpenWhseDialog = function(e, rowIndex) {
        if(!$scope.selectedPartner ){
            return;
        }

        let row = $scope.bean.lines[rowIndex];

        dialogService.openDialog('searchWhseModal', $scope, {itemCode: row.itemCode, rowIndex: rowIndex}, null, function () {
            $timeout(function () {
                let scope = $scope.getScopeById("searchWhseModal");

                scope.init();

                loaderService.hide();
            }, 100);
        });
    };

    $scope.dialogChooseWhse = function(data, rowIndex){

        let row = $scope.bean.lines[rowIndex];

        row.warehouseCode = data.whsCode;

    };

    $scope.doOpenSalesHistDialog = function(e, rowIndex) {
        if(!$scope.selectedPartner ){
            return;
        }

        let row = $scope.bean.lines[rowIndex];

        let dialogCondition = {
            cardCode: $scope.bean.cardCode, 
            itemCode: row.itemCode, 
            rowIndex: rowIndex,
            docEntry: $scope.bean.docEntry,
            isDraft: $scope.bean.isDraft
        };

        dialogService.openDialog('searchSalesPriceHistModal', $scope, dialogCondition, null, function () {
            $timeout(function () {
                let scope = $scope.getScopeById("searchSalesPriceHistModal");

                scope.init();

                loaderService.hide();
            }, 100);
        });
    };

    $scope.doOpenBatchNoDialog = function(e, rowIndex) {
        if(!$scope.selectedPartner ){
            return;
        }

        let row = $scope.bean.lines[rowIndex];

        if(!row.warehouseCode){
            dialogService.showAlertDialog($scope, 'Validate', 'Please choose Warehouse code');

            return;
        }

        dialogService.openDialog('searchBatchNoModal', $scope, {selectedRow: row, rowIndex: rowIndex}, null, function () {
            $timeout(function () {
                let scope = $scope.getScopeById("searchBatchNoModal");

                scope.init();

                loaderService.hide();
            }, 100);
        });
    };

    $scope.dialogChooseBatchNo = function(list, rowIndex){

        let row = $scope.bean.lines[rowIndex];

        row.batchs = list;
    };

    $scope.refreshSeries = function() {
        if($scope.bean.docDate){
            let reqObj = {docDate: $scope.bean.docDate};

            documentService.searchSeries(reqObj).then(function(res){
                $scope.seriesList = res.response;

                if('add'===$scope.mode){
                    if($scope.seriesList && $scope.seriesList.length>0){
                        $scope.bean.series = $scope.seriesList[0].series;
                    }
                }
            });
        }
    };

    $scope.doSelectShipTo = function(e) {

        if($scope.shipToList && $scope.bean.shipToCode){
            angular.forEach($scope.shipToList, function(shipTo){
                if(shipTo.addressName===$scope.bean.shipToCode){
                    $scope.bean.address2 = shipTo.addrName;
                }
            });
        }else{
            $scope.bean.address2 = null;
        }
    };

    $scope.doSelectBillTo = function(e) {

        if($scope.billToList && $scope.bean.payToCode){
            angular.forEach($scope.billToList, function(billTo){
                if(billTo.addressName===$scope.bean.payToCode){
                    $scope.bean.address = billTo.addrName;
                    $scope.bean.bpBrnCode = billTo.bpBrnCode;
                }
            });
        }else{
            $scope.bean.address = null;
            $scope.bean.bpBrnCode = null;
        }

        //$scope.bean.bpBrnCode = $scope.selectedPartner.bpBrnCode;
    };

    $scope.doChangeLineQty = function(e, row, forceChangePrice){
        if(!row.quantity){
            row.quantity = 0;
        }

        //fix decimal place
        row.quantity = $scope.rowQtyDecimalPlace(row.quantity);

        if(forceChangePrice || (row.isSpecialPrice && row.quantity !== 0 && row.uomEntry)){
            let reqObj = {};

            reqObj.CardCode = $scope.bean.cardCode;
            reqObj.ItemCode = row.itemCode;
            reqObj.Quantity = row.quantity;
            reqObj.Date = $scope.bean.docDate;
            reqObj.UomEntry = row.uomEntry;
            reqObj.PriceList = $scope.bean.priceListNum;

            documentService.searchItemPrice(reqObj).then(function(response){
                let res = response.response;
                // row.price = res.price;
                row.unitPrice = res.unitPrice;
                // row.unitPrice = res.price;
                row.discountPercent = res.discount;

                if(res.isSpecialPrice){
                    row.priceAfterDiscount = res.specialPrice;
                }
                
                // row.isSpecialPrice = res.isSpecialPrice;

                $scope.doCalculateLine(e, row);
            });
        }else{
            $scope.doCalculateLine(e, row);
        }
        
    };

    $scope.doChangeLineUom = function(e, row){
        //calculate uom qty
        if($scope.uomGroupList){
            let uomGroupList = $filter('filter')($scope.uomGroupList, function(value){
                return value.ugpEntry === row.ugpEntry;
            });


            for(let i=0;i<uomGroupList.length;i++){
                let uomData = uomGroupList[i];
                
                if(uomData.uomEntry===row.uomEntry){
                    row.uomBaseQty = uomData.baseQty;
                    row.uomAltQty = uomData.altQty;

                    // console.log('found uom row.ugpEntry : {} row.uomEntry : {} row.uomBaseQty : {} row.uomAltQty : {} ', row.ugpEntry, row.uomEntry, row.uomBaseQty, row.uomAltQty);

                    break;
                }
            }

            // console.log('row.ugpEntry : {} row.uomEntry : {} row.uomBaseQty : {} row.uomAltQty : {} ', row.ugpEntry, row.uomEntry, row.uomBaseQty, row.uomAltQty);
        }

        $scope.doChangeLineQty(e, row, true);
    };

    $scope.doChangeLineUnitPrice = function(e, row){
        if(!row.unitPrice){
            row.unitPrice = 0;
        }

        //limit 6 decimal point
        row.unitPrice = numberUtils.fixedDecimalPlaces(row.unitPrice, 6);

        //6. ตาราง item ช่อง priceAfterDiscount = (unitPrice * discountPercent / 100)       ** ถ้า  discountPercent ให้ใส่ unitPrice
        row.priceAfterDiscount = row.unitPrice - numberUtils.fixedDecimalPlaces(((row.unitPrice * row.discountPercent) / 100), 6);
        row.priceAfterDiscount = numberUtils.fixedDecimalPlaces(row.priceAfterDiscount, 6);

        //7. ตาราง item ช่อง grossPrice = priceAfterDiscount + (priceAfterDiscount * taxRate / 100)     *** ถ้า taxRate = 0 ให้ใส่ priceAfterDiscount เลย
        row.priceAfterVat = row.priceAfterDiscount + numberUtils.fixedDecimalPlaces(((row.priceAfterDiscount * row.vatRate) / 100), 2);
        row.priceAfterVat = numberUtils.fixedDecimalPlaces(row.priceAfterVat, 2);

        //8. ตาราง item ช่อง total = priceAfterDiscount
        row.lineTotalAmount = row.priceAfterDiscount * row.quantity;
        row.lineTotalAmount = numberUtils.fixedDecimalPlaces(row.lineTotalAmount, 2);

        //15. ตาราง item ช่อง Tax Amount = (priceAfterDiscount * vatRate / 100)
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.quantity * ((row.priceAfterDiscount * row.vatRate) / 100), 2);
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.vatAmount, 2);

        $scope.doCalculateDiscount(e);
    };

    $scope.doChangeLineDiscountPercent = function(e, row){
        if(!row.discountPercent){
            row.discountPercent = 0;
        }

        //limit decimal point
        row.discountPercent = numberUtils.fixedDecimalPlaces(row.discountPercent, 2);

        //6. ตาราง item ช่อง priceAfterDiscount = (unitPrice * discountPercent / 100)       ** ถ้า  discountPercent ให้ใส่ unitPrice
        row.priceAfterDiscount = row.unitPrice - numberUtils.fixedDecimalPlaces(((row.unitPrice * row.discountPercent) / 100), 6);
        row.priceAfterDiscount = numberUtils.fixedDecimalPlaces(row.priceAfterDiscount, 6);

        //7. ตาราง item ช่อง grossPrice = priceAfterDiscount + (priceAfterDiscount * taxRate / 100)     *** ถ้า taxRate = 0 ให้ใส่ priceAfterDiscount เลย
        row.priceAfterVat = row.priceAfterDiscount + numberUtils.fixedDecimalPlaces((row.priceAfterDiscount * row.vatRate) / 100, 2);
        row.priceAfterVat = numberUtils.fixedDecimalPlaces(row.priceAfterVat, 2);

        //8. ตาราง item ช่อง total = priceAfterDiscount
        row.lineTotalAmount = row.priceAfterDiscount * row.quantity;
        row.vatAmolineTotalAmountunt = numberUtils.fixedDecimalPlaces(row.lineTotalAmount, 2);

        //15. ตาราง item ช่อง Tax Amount = (priceAfterDiscount * vatRate / 100)
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.quantity * ((row.priceAfterDiscount * row.vatRate) / 100), 2);
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.vatAmount, 2);

        $scope.doCalculateDiscount(e);
    };

    $scope.doChangeLineAfterDiscount = function(e, row){
        if(!row.priceAfterDiscount){
            row.priceAfterDiscount = 0;
        }

        //limit 6 decimal point
        row.priceAfterDiscount = numberUtils.fixedDecimalPlaces(row.priceAfterDiscount, 6);

        //ย้อนกลับ discount percent
        row.discountPercent = numberUtils.fixedDecimalPlaces(((row.unitPrice - row.priceAfterDiscount) * 100) / row.unitPrice, 2);
        row.discountPercent = numberUtils.fixedDecimalPlaces(row.discountPercent, 2);

        //7. ตาราง item ช่อง grossPrice = priceAfterDiscount + (priceAfterDiscount * taxRate / 100)     *** ถ้า taxRate = 0 ให้ใส่ priceAfterDiscount เลย
        row.priceAfterVat = row.priceAfterDiscount + numberUtils.fixedDecimalPlaces((row.priceAfterDiscount * row.vatRate) / 100, 2);
        row.priceAfterVat = numberUtils.fixedDecimalPlaces(row.priceAfterVat, 2);

        //8. ตาราง item ช่อง total = priceAfterDiscount
        row.lineTotalAmount = row.priceAfterDiscount * row.quantity;
        row.lineTotalAmount = numberUtils.fixedDecimalPlaces(row.lineTotalAmount, 2);

        //15. ตาราง item ช่อง Tax Amount = (priceAfterDiscount * vatRate / 100)
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.quantity * ((row.priceAfterDiscount * row.vatRate) / 100), 2);
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.vatAmount, 2);

        $scope.doCalculateDiscount(e);
    };

    $scope.doChangeLineGrossing = function(e, row){
        if(!row.priceAfterVat){
            row.priceAfterVat = 0;
        }

        //limit 6 decimal point
        row.priceAfterVat = numberUtils.fixedDecimalPlaces(row.priceAfterVat, 6);

        //7.1 ถ้าใส่ช่อง grossPrice โดย unitPrice = 0 และ discountPercent = 0 จะต้องย้อนหา unitPrice โดยใช้ unitPrice = grossPrice - (grossPrice * taxRate / 100)
        // if(row.unitPrice===0){
            // row.unitPrice = row.priceAfterVat - numberUtils.fixedDecimalPlaces(((row.priceAfterVat * row.vatRate) / 100), 2);
            row.unitPrice = numberUtils.fixedDecimalPlaces((row.priceAfterVat * 100) / (100 + row.vatRate), 6);
            row.unitPrice = numberUtils.fixedDecimalPlaces(row.unitPrice, 6);

            //6. ตาราง item ช่อง priceAfterDiscount = (unitPrice * discountPercent / 100)       ** ถ้า  discountPercent ให้ใส่ unitPrice
            row.priceAfterDiscount = row.unitPrice - numberUtils.fixedDecimalPlaces((row.unitPrice * row.discountPercent) / 100, 6);
            row.priceAfterDiscount = numberUtils.fixedDecimalPlaces(row.priceAfterDiscount, 6);

            //8. ตาราง item ช่อง total = priceAfterDiscount
            row.lineTotalAmount = row.priceAfterDiscount;
            row.lineTotalAmount = numberUtils.fixedDecimalPlaces(row.lineTotalAmount, 2);

            //15. ตาราง item ช่อง Tax Amount = (priceAfterDiscount * vatRate / 100)
            row.vatAmount = numberUtils.fixedDecimalPlaces(row.quantity * ((row.priceAfterDiscount * row.vatRate) / 100), 2);
            row.vatAmount = numberUtils.fixedDecimalPlaces(row.vatAmount, 2);

            $scope.doCalculateDiscount(e);
        // }else{
            //row.priceAfterDiscount = row.priceAfterVat - (row.priceAfterDiscount * row.vatRate / 100);
            //TODO
        // }

    };

    $scope.doCalculateLine = function(e, row){
        //change vatGroup
        if($scope.taxGroupList){
            for(let i=0;i<$scope.taxGroupList.length;i++){
                let taxGroup = $scope.taxGroupList[i];

                if(row.vatGroup===taxGroup.code){
                    row.vatRate = taxGroup.rate;

                    break;
                }
            }
        }

        //6. ตาราง item ช่อง priceAfterDiscount = (unitPrice * discountPercent / 100)       ** ถ้า  discountPercent ให้ใส่ unitPrice
        row.priceAfterDiscount = row.unitPrice - numberUtils.fixedDecimalPlaces((row.unitPrice * row.discountPercent) / 100, 6);
        row.priceAfterDiscount = numberUtils.fixedDecimalPlaces(row.priceAfterDiscount, 6);

        //7. ตาราง item ช่อง grossPrice = priceAfterDiscount + (priceAfterDiscount * taxRate / 100)     *** ถ้า taxRate = 0 ให้ใส่ priceAfterDiscount เลย
        row.priceAfterVat = row.priceAfterDiscount + numberUtils.fixedDecimalPlaces((row.priceAfterDiscount * row.vatRate) / 100, 2);
        row.priceAfterVat = numberUtils.fixedDecimalPlaces(row.priceAfterVat, 2);

        //7.1 ถ้าใส่ช่อง grossPrice โดย unitPrice = 0 และ discountPercent = 0 จะต้องย้อนหา unitPrice โดยใช้ unitPrice = grossPrice - (grossPrice * taxRate / 100)
        if(row.unitPrice===0){
            // row.unitPrice = row.priceAfterVat - numberUtils.fixedDecimalPlaces((row.priceAfterVat * row.vatRate) / 100, 6);
            row.unitPrice = numberUtils.fixedDecimalPlaces((row.priceAfterVat * 100) / (100 + row.vatRate), 6);
            row.unitPrice = numberUtils.fixedDecimalPlaces(row.unitPrice, 6);
        }

        //8. ตาราง item ช่อง total = priceAfterDiscount
        row.lineTotalAmount = row.priceAfterDiscount * row.quantity;
        row.lineTotalAmount = numberUtils.fixedDecimalPlaces(row.lineTotalAmount, 2);

        //15. ตาราง item ช่อง Tax Amount = (priceAfterDiscount * vatRate / 100)
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.quantity * ((row.priceAfterDiscount * row.vatRate) / 100), 2);
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.vatAmount, 2);

        $scope.doCalculateDiscount(e);        
    };

    $scope.doCalculateLineAfterChooseItem = function(e, row){
        //change vatGroup
        if($scope.taxGroupList){
            for(let i=0;i<$scope.taxGroupList.length;i++){
                let taxGroup = $scope.taxGroupList[i];

                if(row.vatGroup===taxGroup.code){
                    row.vatRate = taxGroup.rate;

                    break;
                }
            }
        }

        //6. ตาราง item ช่อง priceAfterDiscount = (unitPrice * discountPercent / 100)       ** ถ้า  discountPercent ให้ใส่ unitPrice
        // row.priceAfterDiscount = row.unitPrice - numberUtils.fixedDecimalPlaces((row.unitPrice * row.discountPercent) / 100, 6);
        // row.priceAfterDiscount = numberUtils.fixedDecimalPlaces(row.priceAfterDiscount, 6);
        //skip cal priceAfterDiscount because cal from server side

        //7. ตาราง item ช่อง grossPrice = priceAfterDiscount + (priceAfterDiscount * taxRate / 100)     *** ถ้า taxRate = 0 ให้ใส่ priceAfterDiscount เลย
        row.priceAfterVat = row.priceAfterDiscount + numberUtils.fixedDecimalPlaces((row.priceAfterDiscount * row.vatRate) / 100, 2);
        row.priceAfterVat = numberUtils.fixedDecimalPlaces(row.priceAfterVat, 2);

        //7.1 ถ้าใส่ช่อง grossPrice โดย unitPrice = 0 และ discountPercent = 0 จะต้องย้อนหา unitPrice โดยใช้ unitPrice = grossPrice - (grossPrice * taxRate / 100)
        if(row.unitPrice===0){
            // row.unitPrice = row.priceAfterVat - numberUtils.fixedDecimalPlaces((row.priceAfterVat * row.vatRate) / 100, 6);
            // row.unitPrice = numberUtils.fixedDecimalPlaces((row.priceAfterVat * 100) / (100 + row.vatRate), 6);
            row.unitPrice = row.priceAfterDiscount * numberUtils.fixedDecimalPlaces((100/(100-row.discountPercent)), 6);
            row.unitPrice = numberUtils.fixedDecimalPlaces(row.unitPrice, 6);
        }

        //8. ตาราง item ช่อง total = priceAfterDiscount
        row.lineTotalAmount = row.priceAfterDiscount * row.quantity;
        row.lineTotalAmount = numberUtils.fixedDecimalPlaces(row.lineTotalAmount, 2);

        //15. ตาราง item ช่อง Tax Amount = (priceAfterDiscount * vatRate / 100)
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.quantity * ((row.priceAfterDiscount * row.vatRate) / 100), 2);
        row.vatAmount = numberUtils.fixedDecimalPlaces(row.vatAmount, 2);

        $scope.doCalculateDiscount(e);        
    };

    $scope.doCalculateDiscount = function(e, byAmount){

        //cal totalbeforediscount
        {
            $scope.bean.totalBeforeDiscount = 0.00;

            for(let i=0;i<$scope.bean.lines.length;i++){
                let line = $scope.bean.lines[i];
                $scope.bean.totalBeforeDiscount = numberUtils.fixedDecimalPlaces($scope.bean.totalBeforeDiscount + line.lineTotalAmount, 2);
            }

            $scope.bean.totalBeforeDiscount = numberUtils.fixedDecimalPlaces($scope.bean.totalBeforeDiscount, 2);
        }

        if(byAmount){
            //12. header ช่อง Discount Amount เมื่อกรอกจะคำนวนหา Discount % โดยใช้สูตร Discount % = (discountAmount / totalBeforeDiscount * 100)
            $scope.bean.discountPercent = numberUtils.fixedDecimalPlaces($scope.bean.discountAmount / $scope.bean.totalBeforeDiscount * 100, 2);
        }else{
            //11. header ช่อง Discount % เมื่อกรอกจะคำนวนหา Discount Amount โดยใช้สูตร Discount Amount = (totalBeforeDiscount * discountPercent / 100)  ** ต้องเก็บ 2 อัน 1.ทศนิยม 2 ตำแหน่งเพื่อแสดง 2. ทศนิยม 6 ตำแหน่งเพื่อบันทึก
            $scope.bean.discountAmount = numberUtils.fixedDecimalPlaces(($scope.bean.discountPercent * $scope.bean.totalBeforeDiscount) / 100, 2);
        }

        $scope.doCalculateTotal(e);
    };

    $scope.doCalculateTotal = function(e){
        //13. header ช่อง Tax = (sum(item.vatAmount) - (sum(item.vatAmount) * discountPercent / 100))
        {
            let sumVatAmount = 0.00;

            for(let i=0;i<$scope.bean.lines.length;i++){
                let line = $scope.bean.lines[i];

                sumVatAmount = sumVatAmount + line.vatAmount;
            }

            $scope.bean.vatAmount = sumVatAmount - (sumVatAmount * ($scope.bean.discountPercent / 100.00));
            $scope.bean.vatAmount = numberUtils.fixedDecimalPlaces($scope.bean.vatAmount, 2);
        }

        //14. header ช่อง Total = (totalBeforeDiscount - discountAmount + tax)
        {
            $scope.bean.docTotal = numberUtils.fixedDecimalPlaces($scope.bean.totalBeforeDiscount - $scope.bean.discountAmount + $scope.bean.vatAmount, 2);
        }


    };

    $scope.doAddLine = function(e){

        if(!$scope.selectedPartner){
            return;
        }

        if(!$scope.bean.lines){
            $scope.bean.lines = [];
        }

        let newItem = {};

        newItem.lineNum = --$scope.newLineIndex;
        newItem.discountPercent = 0;
        newItem.priceAfterDiscount = 0;
        newItem.vatAmount = 0;
        newItem.vatRate = 0;
        newItem.priceAfterVat = 0;
        newItem.lineTotalAmount = 0;
        newItem.freeSampling = 'N';

        $scope.bean.lines.push(newItem);
    };

    $scope.doRemoveLine = function(e, index){
        if ($scope.bean && $scope.bean.lines) {
            $scope.bean.lines.splice(index, 1);
        }
        $scope.doCalculateDiscount();
    };

    $scope.doUpload = function(){
        $scope.showUpload = !$scope.showUpload;
    };
    $scope.onUploadFile = function(file) {
        if(file==null || file.size > appConfig.uploadMaxSize){
            dialogService.showAlertDialog($scope, 'Invalid', 'Not allow file size over 5MB.');
        }else{
            loaderService.show();

            let fd = new FormData();

            fd.append('file', file);

            uploadService.uploadFile(fd).then(function(res){

                // console.log(res);
                if(!$scope.bean.attachments) {
                    $scope.bean.attachments = [];
                }

                $scope.bean.attachments.push(res);

                loaderService.hide();
            }, function(err){
                exceptionService.handler($scope, err);
                loaderService.hide();
            }).finally(function(){
                $scope.showUpload = false;
            });
        }
    };
    $scope.doDeleteFile = function(e, index, fileName){
        if($scope.bean.attachments){
            if($scope.bean.attachments.length>index){
                $scope.bean.attachments.splice(index, 1);
            }
        }
    };
    $scope.doDownloadFile = function(e, index, fileObj){
        let url = null;

        if(fileObj.tempKey){
            url = uploadService.getDownloadTempFileLink(fileObj.tempKey);
        }else{
            url = uploadService.getDownloadFileLink(fileObj.fileName);
        }

        $scope.onHref(url);
    };
    $scope.getApproveStatusName = function(statusCode){

        if(!$scope.approveStatusList){
            return;
        }

        for(let i=0;i<$scope.approveStatusList.length;i++){
            let constant = $scope.approveStatusList[i];

            if(constant.code===statusCode){
                return constant.name;
            }
        }

        // return constantService.getConstantLabelByValue($scope.approveStatusList, statusCode);
    };

    $scope.rowQtyDecimalPlace = function(qty){
        // console.log(e);

        if(qty && qty !== 0){
            let fixStr = qty.toFixed($scope.orderQtyDecimalPlace);

            return Number(fixStr);
        }

        return qty;
    };

    $scope.doExport = function(e) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        let cond = {};

        cond.docEntry = $scope.bean.docEntry;
        cond.isDraft = $scope.bean.isDraft;

        reportService.getReportSalesOrder(cond).then(function(response) {

            loaderService.hide();

            deferred.resolve(response);

            $scope.downloadReport(response);
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
        loaderService.hide();
    });
});