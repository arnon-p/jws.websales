"use strict";

angular.module('NwsFront').controller('invStatusSearchCtrl', function ($scope, masterService, constantService, localStorageConfig, storageService, loaderService){
	//declare variable
    $scope.condition = null;

    $scope.itemGroupList = null;
    $scope.whseList = null;

    $scope.whseExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

    //declare function
    $scope.initForm = function() {
        $scope.condition = {selectedWhse: []};

        //constant
        if(!$scope.itemGroupList){
            $scope.itemGroupList = constantService.getItemGroup();
        }

        if(!$scope.whseList){
            let whseCodeList = constantService.getWhse();

            $scope.whseList = [];

            for(var i=0;i<whseCodeList.length;i++){
                let whseCode = whseCodeList[i];

                let whse = {};

                whse.id = whseCode;
                whse.label = whseCode;
                whse.value = i;

                $scope.whseList.push(whse);
            }
        }
    };
    $scope.getSelectedWhse = function() {
        if($scope.condition.selectedWhse && $scope.condition.selectedWhse.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedWhse, function(whse){
                result.push(whse.id);
            });

            return result;
        }else{
            return null;
        }
    };
    $scope.gotoDetail = function(e) {
        loaderService.show(true);
        
        let cond = angular.copy($scope.condition);

        delete cond['selectedWhse'];

        cond.whsCode = $scope.getSelectedWhse();

        storageService.setData(localStorageConfig.editSubBean, cond);

        $scope.gotoState('app.sales.invstatusedit');
    };
    $scope.cardAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});