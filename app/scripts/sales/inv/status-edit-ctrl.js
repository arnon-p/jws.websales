"use strict";

angular.module('NwsFront').controller('invStatusEditCtrl', function ($scope, $q, loaderService, exceptionService, reportService, storageService, localStorageConfig){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.localCondition = null;
    // $scope.paginable = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = storageService.getData(localStorageConfig.editSubBean);

        $scope.doSearch();
    };

    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        reportService.searchInventoryStatus($scope.condition).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            // $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

	$scope.gotoDetail = function(e, data) {

        data.whsCode = $scope.condition.whsCode;

        loaderService.show(true);
        storageService.setData(localStorageConfig.editDialogBean, data);
        $scope.gotoState('app.sales.invstatusdoc');
    };

    $scope.sumOnHand = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onHand;
        });

        return sum;
    };

    $scope.sumIsCommited = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.isCommited;
        });

        return sum;
    };

    $scope.sumOnOrder = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onOrder;
        });

        return sum;
    };

    $scope.sumAvailable = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.avaliable;
        });

        return sum;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();

        loaderService.hide();
    });
});