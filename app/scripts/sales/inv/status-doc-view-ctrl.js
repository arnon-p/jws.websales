"use strict";

angular.module('NwsFront').controller('invStatusDocViewCtrl', function ($scope, $q, loaderService, exceptionService, reportService, storageService, localStorageConfig){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    // $scope.paginable = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = storageService.getData(localStorageConfig.editDialogBean);

        $scope.doSearch();
    };

    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        reportService.getInventoryStatusDetail($scope.condition.itemCode).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            // $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.sumIsCommited = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.isCommited;
        });

        return sum;
    };

    $scope.sumOnOrder = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onOrder;
        });

        return sum;
    };

    $scope.gotoOrderDetail = function(e, row) {
        if(row.objType!=="17"){
            return;
        }

        loaderService.show(true);

        if(row){
            let data = angular.copy(row);

            data.docDate = null;
            data.docDueDate = null;
            data.taxDate = null;
            data.statusDocView = true;

            storageService.setData(localStorageConfig.editBean, data);
        }else{
            storageService.setData(localStorageConfig.editBean, null);
        }
        
        $scope.gotoState('app.sales.arorderedit');
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();

        loaderService.hide();
    });
});