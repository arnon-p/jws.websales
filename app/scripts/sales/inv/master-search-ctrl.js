"use strict";

angular.module('NwsFront').controller('invMasterSearchCtrl', function ($scope, constantService, localStorageConfig, storageService, exceptionService, loaderService, masterService, $q, Pagination){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.paginable = null;

    $scope.itemGroupList = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = {};
        $scope.resultList = null;
        $scope.paginable = Pagination.getDefaultPaginable();

        if(!$scope.itemGroupList){
            $scope.itemGroupList = constantService.getItemGroup();
        }
    };
    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        masterService.searchItem($scope.condition).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };
    $scope.gotoDetail = function(e, data) {
        loaderService.show(true);
        storageService.setData(localStorageConfig.editBean, data);
        $scope.gotoState('app.sales.invmasteredit');
    };
    $scope.doDeleteResultList = function() {
        var deferred = $q.defer();
        loaderService.show();
        var checkedIds = $scope.getAllCheckedDeleting();
        if (checkedIds != null && checkedIds.length > 0) {
            loaderService.hide();
            dialogService.confirmDeleteDialogWithMessage($scope, function() {
                dialogService.closeDeleteDialog();
                salesService.deleteDepartmentById(checkedIds).then(function(response) {
                    deferred.resolve(response);
                    loaderService.hide();
                    dialogService.deleteSuccess($scope);
                }, function(error) {
                    loaderService.hide();
                    deferred.reject(error);
                    exceptionService.queryHandler($scope, error);
                }).finally(function() {
                    loaderService.hide();
                    $scope.condition = {};
                    $scope.doSearch();
                });
                return deferred.promise;
            },'common.confirmdelete.invmaster');
        } else {
            loaderService.hide();
            dialogService.warningSelectRecord($scope);
        }
    };
    $scope.getAllCheckedDeleting = function() {
        if ($scope.resultList) {
            var checkedDeleting = [];
            for (var i = 0; i < $scope.resultList.length; i++) {
                var row = $scope.resultList[i];
                if (row.checked) {
                    checkedDeleting.push(row.departmentId);
                }
            }
            return checkedDeleting;
        }
        return null;
    };
    $scope.toggleObjSelection = function($event, description) {
        $event.stopPropagation();
    };
    $scope.markAllResultIsClicked = function(e) {
        angular.forEach($scope.resultList, function(itm) {
            itm.checked = event.target.checked;
        });
    };
    $scope.itemAutoOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoItem(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.itemCode + ' ' + item.itemName;
            return {
                value: item.itemCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});