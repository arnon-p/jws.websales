"use strict";

angular.module('NwsFront').controller('batchNumberReportViewCtrl', function ($scope, $q, loaderService, exceptionService, reportService, storageService, localStorageConfig){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.localCondition = null;
    // $scope.paginable = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = storageService.getData(localStorageConfig.editBean);

        $scope.doSearch();
    };

    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        reportService.searchBatchNumber($scope.condition).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            // $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

	$scope.gotoDetail = function(e, data) {
        loaderService.show(true);

        let detailCondition = angular.copy(data);

        detailCondition.cardCodeFrom = $scope.condition.cardCodeFrom;
        detailCondition.cardCodeTo = $scope.condition.cardCodeTo;

        storageService.setData(localStorageConfig.editDialogBean, detailCondition);
        $scope.gotoState('app.sales.invbatchnumberdocview');
    };

    $scope.sumQty = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.quantity;
        });

        return sum;
    };
    $scope.sumAllocate = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.commitQty;
        });

        return sum;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();

        loaderService.hide();
    });
});