"use strict";

angular.module('NwsFront').controller('batchNumberDocViewCtrl', function ($scope, $q, loaderService, exceptionService, reportService, storageService, localStorageConfig){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    // $scope.paginable = null;

    //declare function
    $scope.initForm = function() {
        $scope.condition = storageService.getData(localStorageConfig.editDialogBean);

        $scope.doSearch();
    };

    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        let reqCondition = {
            itemCode: $scope.condition.itemCode, 
            batchNumber: $scope.condition.distNumber, 
            whsCode: $scope.condition.whsCode,
            cardCodeFrom: $scope.condition.cardCodeFrom,
            cardCodeTo: $scope.condition.cardCodeTo
        };

        reportService.getBatchNumberDetail(reqCondition).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            // $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.sumQty = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.quantity;
        });

        return sum;
    };
    $scope.sumAllocate = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.allocQty;
        });

        return sum;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();

        loaderService.hide();
    });
});