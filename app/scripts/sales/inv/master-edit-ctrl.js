"use strict";

angular.module('NwsFront').controller('invMasterEditCtrl', function ($scope, $q, constantService, exceptionService, masterService, loaderService, storageService, localStorageConfig){
	//declare variable
    $scope.bean = null;
    $scope.tabIndex = 0;
    $scope.showUdf = false;

    $scope.itemGroupList = null;

    //declare function
    $scope.initForm = function() {
        //constant
        if(!$scope.itemGroupList){
            $scope.itemGroupList = constantService.getItemGroup();
        }

        $scope.bean = storageService.getData(localStorageConfig.editBean);
        if (!$scope.bean) { //add
            $scope.bean = {};
        } else {
            loaderService.show();

            masterService.getItemByCode($scope.bean.itemCode).then(function(response) {
                $scope.bean = response;
                loaderService.hide();
            }, function(error) {
                $scope.bean = null;
                loaderService.hide();
                exceptionService.queryHandler($scope, error);
            });
        }
    };
    $scope.toggleTab = function (e, index) {
        $scope.tabIndex = index;
    };
    $scope.toggleUdf = function(e){
        $scope.showUdf = !$scope.showUdf;
    };
    // <td class="txt-c">{{row.onHand}}</td>
    //                                     <td class="txt-c">{{row.isCommited}}</td>
    //                                     <td class="txt-c">{{row.onOrder}}</td>
    //                                     <td class="txt-c">{{row.available}}</td>
    $scope.sumOnHand = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onHand;
        });

        return sum;
    };
    $scope.sumIsCommited = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.isCommited;
        });

        return sum;
    };
    $scope.sumOnOrder = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onOrder;
        });

        return sum;
    };
    $scope.sumAvailable = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.available;
        });

        return sum;
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
        loaderService.hide();
    });
});