"use strict";

angular.module('NwsFront').controller('bpMasterSearchCtrl', function ($scope, constantService, exceptionService, dialogService, localStorageConfig, storageService, loaderService, $q, Pagination, masterService){
	//declare variable
    $scope.condition = null;
    $scope.resultList = null;
    $scope.paginable = null;

    $scope.salesEmployeeList = null;

    $scope.salesExSetting = { scrollable: true, smartButtonMaxItems: 3, selectedToTop: true, idProperty: 'id'};

    //declare function
    $scope.initForm = function() {
        $scope.condition = {selectedSales: []};
        $scope.resultList = null;
        $scope.paginable = Pagination.getDefaultPaginable();

        //constant
        if(!$scope.salesEmployeeList){
            let salesEmployees = constantService.getSalesEmployee();

            $scope.salesEmployeeList = [];

            for(let i=0;i<salesEmployees.length;i++){
                let sales = salesEmployees[i];

                let sale = {};

                sale.id = sales.slpCode;
                sale.label = sales.slpName;
                sale.value = i;
                sale.name = sales.slpName;

                $scope.salesEmployeeList.push(sale);
            }
        }
    };
    $scope.doSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.condition.paginable = Pagination.getPaginable($scope.paginable, page);

        let cond = angular.copy($scope.condition);

        delete cond['selectedSales'];

        cond.slpCode = $scope.getSelectedSales();

        masterService.searchBp(cond).then(function(response) {
            deferred.resolve(response);
            $scope.resultList = response.response;
            $scope.paginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.resultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };
    $scope.getSelectedSales = function() {
        if($scope.condition.selectedSales && $scope.condition.selectedSales.length>0){
            let result = [];

            angular.forEach($scope.condition.selectedSales, function(sale){
                result.push(sale.id);
            });

            return result;
        }else{
            return null;
        }
    };
    $scope.gotoDetail = function(e, data) {
        loaderService.show(true);
        storageService.setData(localStorageConfig.editBean, data);
        $scope.gotoState('app.sales.bpmasteredit');
    };
    $scope.doDeleteResultList = function() {
        var deferred = $q.defer();
        loaderService.show();
        var checkedIds = $scope.getAllCheckedDeleting();
        if (checkedIds != null && checkedIds.length > 0) {
            loaderService.hide();
            dialogService.confirmDeleteDialogWithMessage($scope, function() {
                dialogService.closeDeleteDialog();
                salesService.deleteDepartmentById(checkedIds).then(function(response) {
                    deferred.resolve(response);
                    loaderService.hide();
                    dialogService.deleteSuccess($scope);
                }, function(error) {
                    loaderService.hide();
                    deferred.reject(error);
                    exceptionService.queryHandler($scope, error);
                }).finally(function() {
                    loaderService.hide();
                    $scope.condition = {};
                    $scope.doSearch();
                });
                return deferred.promise;
            },'common.confirmdelete.bpmaster');
        } else {
            loaderService.hide();
            dialogService.warningSelectRecord($scope);
        }
    };
    $scope.getAllCheckedDeleting = function() {
        if ($scope.resultList) {
            var checkedDeleting = [];
            for (var i = 0; i < $scope.resultList.length; i++) {
                var row = $scope.resultList[i];
                if (row.checked) {
                    checkedDeleting.push(row.departmentId);
                }
            }
            return checkedDeleting;
        }
        return null;
    };
    $scope.toggleObjSelection = function($event, description) {
        $event.stopPropagation();
    };
    $scope.markAllResultIsClicked = function(e) {
        angular.forEach($scope.resultList, function(itm) {
            itm.checked = event.target.checked;
        });
    };
    $scope.getSalesEmployeeName = function(code){
        for(let i=0;i<$scope.salesEmployeeList.length;i++){
            let constant = $scope.salesEmployeeList[i];

            if(constant.id===code){
                return constant.name;
            }
        }
        return null;
    };
    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
    });
});