"use strict";

angular.module('NwsFront').controller('bpMasterEditCtrl', function ($scope, $filter, appConfig, dialogService, $q, constantService, uploadService, exceptionService, masterService, loaderService, localStorageConfig, storageService){
	//declare variable
    $scope.mode = null;
    $scope.bean = null;
    $scope.tabIndex = 0;
    $scope.showUdf = false;
    $scope.selectedContactPerson = null;
    $scope.selectedAddress = null;
    $scope.selectedAddressHeader = null;
    $scope.tempNewContactPersonName = 'Define New';
    $scope.tempNewAddressId = 'Define New';
    $scope.showUpload = false;

    $scope.shippingTypeList = null;
    $scope.salesEmployeeList = null;
    $scope.bpGroupList = null;
    $scope.countryList = null;
    $scope.paymentTermsList = null;
    $scope.propertiesList = null;
    $scope.stateList = null;
    $scope.priceList = null;
    // $scope.addressList = null;

    //declare function
    $scope.initForm = function() {
        $scope.bean = null;
        $scope.tabIndex = 0;
        $scope.showUdf = false;
        $scope.selectedContactPerson = null;
        $scope.selectedAddress = null;
        $scope.selectedAddressHeader = null;
        $scope.file = null;

        //constant
        if(!$scope.shippingTypeList){
            $scope.shippingTypeList = constantService.getShippingType();
        }
        if(!$scope.salesEmployeeList){
            $scope.salesEmployeeList = constantService.getSalesEmployee();
        }
        if(!$scope.bpGroupList){
            $scope.bpGroupList = constantService.getBpGroup();
        }
        if(!$scope.countryList){
            $scope.countryList = constantService.getCountry();
        }
        if(!$scope.paymentTermsList){
            $scope.paymentTermsList = constantService.getPaymentTerms();
        }
        if(!$scope.propertiesList){
            $scope.propertiesList = constantService.getProperties();
        }
        if(!$scope.priceList){
            $scope.priceList = constantService.getPriceList();
        }
        // if(!$scope.addressList){
        //     $scope.addressList = constantService.getLocationAddress();
        // }

        $scope.bean = storageService.getData(localStorageConfig.editBean);
        if (!$scope.bean) { //add
            $scope.bean = {
                status: 'Inactive'
            };

            $scope.mode = 'add';
        } else {
            loaderService.show();

            masterService.getBpByCardCode($scope.bean.cardCode).then(function(response) {
                $scope.bean = response;

                //calculate propertyList
                if(response.propertyList){
                    for(let i=0;i<$scope.propertiesList.length;i++){
                        let prop = $scope.propertiesList[i];
                        let beanProp = response.propertyList[i];

                        prop.checked = beanProp;
                    }
                }

                loaderService.hide();
            }, function(error) {
                $scope.bean = null;
                loaderService.hide();
                exceptionService.queryHandler($scope, error);
            });

            $scope.mode = 'edit';
        }
    };
    $scope.doSave = function(e) {

        try{
            var deferred = $q.defer();
            loaderService.show();
            var savePromise = null;
            
            let reqObj = angular.copy($scope.bean);

            //set proplist
            reqObj.propertyList = $scope.getPropertyList();

            if ('edit'===$scope.mode) {
                savePromise = masterService.editBp(reqObj);
            } else {
                savePromise = masterService.addBp(reqObj);
            }

            savePromise.then(function(res) {
                deferred.resolve(res);
                //$scope.bean = res;

                storageService.setData(localStorageConfig.editBean, $scope.bean);

                $scope.initForm();
                loaderService.hide();
                dialogService.saveSuccess($scope);
            }, function(error) {
                loaderService.hide();
                deferred.reject(error);
                exceptionService.handler($scope, error);
            });
            return deferred.promise;
        }catch(e){
            console.log(' error doSave ' + e);
        }
    };
    $scope.toggleTab = function (e, index) {
        $scope.tabIndex = index;
    };
    $scope.toggleUdf = function(e){
        $scope.showUdf = !$scope.showUdf;
    };
    $scope.doAddContactPerson = function(e){
        if(!$scope.bean.bpContactList) {
            $scope.bean.bpContactList = [];
        }else{
            //validate dup new
            for(var i=0;i<$scope.bean.bpContactList.length;i++){
                var row = $scope.bean.bpContactList[i];

                if($scope.tempNewContactPersonName===row.name){
                    return false;
                }
            }
        }

        var newContact = {name : $scope.tempNewContactPersonName};

        $scope.bean.bpContactList.push(newContact);

        $scope.doSelectContactPerson(newContact);
    };
    $scope.doSelectContactPerson = function(e, contactPerson){
        $scope.selectedContactPerson = contactPerson;
    };
    $scope.doRemoveContactPerson = function(e, name){
        $scope.bean.bpContactList = $scope.bean.bpContactList.filter(function(value, index, arr){
            return value.name!==name;
        });

        $scope.selectedContactPerson = null;
    };
    $scope.doAddBillTo = function(e, defaultBill){
        if(!$scope.bean.bpBillList) {
            $scope.bean.bpBillList = [];
        }else{
            //validate dup new
            for(let i=0;i<$scope.bean.bpBillList.length;i++){
                let row = $scope.bean.bpBillList[i];

                if($scope.tempNewAddressId===row.address){
                    return false;
                }
            }
        }

        let billTo = defaultBill ? defaultBill : {address : $scope.tempNewAddressId};

        $scope.bean.bpBillList.push(billTo);

        $scope.doSelectBillTo(billTo);
    };
    $scope.doRemoveBillTo = function(e, address){
        $scope.bean.bpBillList = $scope.bean.bpBillList.filter(function(value, index, arr){
            return value.address!==address;
        });

        $scope.selectedAddress = null;

        $scope.selectedAddressHeader = null;
    };
    $scope.doCopyToBill = function(e, addr){

        let billAddr = angular.copy(addr);

        $scope.doAddBillTo(e, billAddr);
    };
    $scope.doSelectBillTo = function(e, billTo){
        $scope.selectedAddress = billTo;

        $scope.selectedAddressHeader = 'Bill To';

        // if($scope.selectedAddress){
        //     $scope.doRefreshStateList(e, $scope.selectedAddress.country);
        // }
        // if($scope.selectedAddress){
        //     $scope.selectedAddress.fullAddr = $scope.selectedAddress.tambon + ' ' + $scope.selectedAddress.amphur + ' ' + $scope.selectedAddress.city + ' ' + $scope.selectedAddress.zipCode;

        //     if($scope.selectedAddress.fullAddr){
        //         $scope.selectedAddress.fullAddr = $scope.selectedAddress.fullAddr.trim();
        //     }
        // }
        $scope.formatFullAddr($scope.selectedAddress);
    };
    $scope.formatFullAddr = function(addr){
        if(addr) {
            addr.fullAddr = '';

            if(addr.tambon){
                addr.fullAddr = addr.fullAddr + addr.tambon + ' ';
            }

            if(addr.amphur){
                addr.fullAddr = addr.fullAddr + addr.amphur + ' ';
            }

            if(addr.city){
                addr.fullAddr = addr.fullAddr + addr.city + ' ';
            }

            if(addr.zipCode){
                addr.fullAddr = addr.fullAddr + addr.zipCode + ' ';
            }

            if(addr.fullAddr){
                addr.fullAddr = addr.fullAddr.trim();
            }
        }
    };
    $scope.doAddShipTo = function(e, defaultShip){
        if(!$scope.bean.bpShipList) {
            $scope.bean.bpShipList = [];
        }else{
            //validate dup new
            for(let i=0;i<$scope.bean.bpShipList.length;i++){
                let row = $scope.bean.bpShipList[i];

                if($scope.tempNewAddressId===row.address){
                    return false;
                }
            }
        }

        let shipTo = defaultShip ? defaultShip : {address : $scope.tempNewAddressId};

        $scope.bean.bpShipList.push(shipTo);

        $scope.doSelectShipTo(shipTo);
    };
    $scope.doRemoveShipTo = function(e, address){
        $scope.bean.bpShipList = $scope.bean.bpShipList.filter(function(value, index, arr){
            return value.address!==address;
        });

        $scope.selectedAddress = null;

        $scope.selectedAddressHeader = null;
    };
    $scope.doCopyToShip = function(e, addr){

        let shipAddr = angular.copy(addr);
        
        $scope.doAddShipTo(e, shipAddr);
    };
    $scope.doSelectShipTo = function(e, shipTo){
        $scope.selectedAddress = shipTo;

        $scope.selectedAddressHeader = 'Ship To';

        // if($scope.selectedAddress){
        //     $scope.doRefreshStateList(e, $scope.selectedAddress.country);
        // }
        // if($scope.selectedAddress){
        //     $scope.selectedAddress.fullAddr = $scope.selectedAddress.tambon + ' ' + $scope.selectedAddress.amphur + ' ' + $scope.selectedAddress.city + ' ' + $scope.selectedAddress.zipCode;

        //     if($scope.selectedAddress.fullAddr){
        //         $scope.selectedAddress.fullAddr = $scope.selectedAddress.fullAddr.trim();
        //     }
        // }
        $scope.formatFullAddr($scope.selectedAddress);
    };
    $scope.doClickProperties = function(e, property){
        property.checked = !property.checked;
    };
    $scope.getPropertyList = function() {
        let propertyList = [];

        angular.forEach($scope.propertiesList, function(prop){
            propertyList.push(prop.checked ? true : false);
        });

        return propertyList;
    };
    $scope.doRefreshStateList = function(e, countryCode){
        let stateList = constantService.getState();

        if(countryCode){
            $scope.stateList = $filter('filter')(stateList, function(value){
                return value.country === countryCode;
            });
        }else{
            $scope.stateList = null;
        }
        
    };
    $scope.doUpload = function(){
        $scope.showUpload = !$scope.showUpload;
    };
    $scope.onUploadFile = function(file) {
        if(file==null || file.size > appConfig.uploadMaxSize){
            dialogService.showAlertDialog($scope, 'Invalid', 'Not allow file size over 5MB.');
        }else{
            loaderService.show();

            let fd = new FormData();

            fd.append('file', file);

            uploadService.uploadFile(fd).then(function(res){

                // console.log(res);
                if(!$scope.bean.attachments) {
                    $scope.bean.attachments = [];
                }

                $scope.bean.attachments.push(res);

                loaderService.hide();
            }, function(err){
                exceptionService.handler($scope, err);
                loaderService.hide();
            }).finally(function(){
                $scope.showUpload = false;
            });
        }
    };
    $scope.doDeleteFile = function(e, index, fileName){
        if($scope.bean.attachments){
            if($scope.bean.attachments.length>index){
                $scope.bean.attachments.splice(index, 1);
            }
        }
    };
    $scope.doDownloadFile = function(e, index, fileObj){
        let url = null;

        if(fileObj.tempKey){
            url = uploadService.getDownloadTempFileLink(fileObj.tempKey);
        }else{
            url = uploadService.getDownloadFileLink(fileObj.fileName);
        }

        $scope.onHref(url);
    };
    $scope.consolidateOptions = {
        minimumChars: 3,
        data: function (searchText) {
            searchText = searchText.toUpperCase();

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            item.compDisplay = item.cardCode + ' ' + item.cardName;
            return {
                value: item.cardCode,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.compDisplay'></p>"
            };
        }
    };
    $scope.addressOptions = {
        minimumChars: 3,
        data: function (searchText) {
            // searchText = searchText.toUpperCase();
            let addressList = constantService.getLocationAddress();

            return $q(function(resolve, reject) {
                try {
                    let resultList = $filter('filter')(addressList, function(value){
                        return value.fullAddr.includes(searchText);
                    });

                    resolve(resultList);
                } catch (e) {
                    reject(e);
                }
            });

            return masterService.searchAutoBp(searchText).then(function(res){
                return res.response;
            });
        },
        renderItem: function (item) {
            return {
                value: item.fullAddr,
                // label: item.cardCode + ' ' + item.cardName
                label: "<p class='auto-complete' ng-bind-html='entry.item.fullAddr'></p>"
            };
        },
        itemSelected: function (e) {
            // console.log('itemSelected : ' + e);

            $scope.selectedAddress.city = e.item.city;
            $scope.selectedAddress.amphur = e.item.amphur;
            $scope.selectedAddress.tambon = e.item.tambon;
            $scope.selectedAddress.zipCode = e.item.zipCode;
        }
    };

    //event
    $scope.$on('$viewContentLoaded', function() {
        $scope.initForm();
        loaderService.hide();
    });
});