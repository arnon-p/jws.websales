"use strict";

angular.module('NwsFront').controller('searchSalesPriceHistDialogCtrl', function ($scope, $q, dateTimeUtils, documentService, dialogService, Pagination, loaderService, exceptionService) {

	//declare variable
    $scope.parentBean = null;
	$scope.dialogCondition = null;
	$scope.dialogResultList = null;
	$scope.dialogPaginable = null;
    $scope.selectedRowIndex = null;

	//declare function
	$scope.init = function () {
		$scope.dialogCondition = {};
		$scope.dialogPaginable = Pagination.getDefaultPaginable();
        $scope.dialogResultList = null;

        if($scope.modal.bean){
            $scope.selectedRowIndex = $scope.modal.bean.rowIndex;

            $scope.dialogCondition.cardCode = $scope.modal.bean.cardCode;
            $scope.dialogCondition.itemCode = $scope.modal.bean.itemCode;
            $scope.dialogCondition.docEntry = $scope.modal.bean.docEntry;
            $scope.dialogCondition.isDraft = $scope.modal.bean.isDraft;
        }

        $scope.doDialogSearch(null, 0);
	};

	$scope.doDialogSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.dialogCondition.dialogPaginable = Pagination.getPaginable($scope.dialogPaginable, page);
        $scope.dialogCondition.paginable = $scope.dialogCondition.dialogPaginable;

        documentService.searchSalesPriceHist($scope.dialogCondition).then(function(response) {
            deferred.resolve(response);
            $scope.dialogResultList = response.response;
            $scope.dialogPaginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.dialogResultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };
	
	//event
	$scope.$on('$viewContentLoaded', function () {
		//TODO
	});
});