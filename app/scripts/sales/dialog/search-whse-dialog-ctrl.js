"use strict";

angular.module('NwsFront').controller('searchWhseDialogCtrl', function ($scope, $q, dateTimeUtils, documentService, dialogService, Pagination, loaderService, exceptionService) {

	//declare variable
    $scope.parentBean = null;
	$scope.dialogCondition = null;
	$scope.dialogResultList = null;
	$scope.dialogPaginable = null;
    $scope.selectedRowIndex = null;

	//declare function
	$scope.init = function () {
		$scope.dialogCondition = {};
		$scope.dialogPaginable = Pagination.getDefaultPaginable();
        $scope.dialogResultList = null;

        if($scope.modal.bean){
            $scope.selectedRowIndex = $scope.modal.bean.rowIndex;

            $scope.dialogCondition.itemCode = $scope.modal.bean.itemCode;
        }

        $scope.doDialogSearch(null, 0);
	};

	$scope.doDialogSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.dialogCondition.dialogPaginable = Pagination.getPaginable($scope.dialogPaginable, page);
        $scope.dialogCondition.paginable = $scope.dialogCondition.dialogPaginable;

        documentService.searchItemWhsOnHand($scope.dialogCondition).then(function(response) {
            deferred.resolve(response);
            $scope.dialogResultList = response.response;
            $scope.dialogPaginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.dialogResultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.doChooseRow = function(e, row){
    	//set data to parent
    	$scope.$parent.dialogChooseWhse(row, $scope.selectedRowIndex);

    	dialogService.closeDialog('searchWhseModal', function () {
			loaderService.hide();
		});

        // $scope.modal = {};
    };

    $scope.sumOnHand = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onHand;
        });

        return sum;
    };

    $scope.sumIsCommited = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.isCommited;
        });

        return sum;
    };

    $scope.sumOnOrder = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.onOrder;
        });

        return sum;
    };

    $scope.sumAvailable = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = sum + item.avaliable;
        });

        return sum;
    };
	
	//event
	$scope.$on('$viewContentLoaded', function () {
		//TODO
	});
});