"use strict";

angular.module('NwsFront').controller('searchPartnerDialogCtrl', function ($scope, $q, documentService, dialogService, Pagination, loaderService, exceptionService) {

	//declare variable
	$scope.dialogCondition = null;
	$scope.dialogResultList = null;
	$scope.dialogPaginable = null;

	//declare function
	$scope.init = function () {
		$scope.dialogCondition = {};
		$scope.dialogPaginable = Pagination.getDefaultPaginable();
        $scope.dialogResultList = null;
	};

	$scope.doDialogSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.dialogCondition.dialogPaginable = Pagination.getPaginable($scope.dialogPaginable, page);
        $scope.dialogCondition.paginable = $scope.dialogCondition.dialogPaginable;

        documentService.searchBpForSO($scope.dialogCondition).then(function(response) {
            deferred.resolve(response);
            $scope.dialogResultList = response.response;
            $scope.dialogPaginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.dialogResultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.doChooseRow = function(e, row){
    	//set data to parent
    	$scope.$parent.dialogChoosePartner(row);

    	dialogService.closeDialog('searchPartnerModal', function () {
			loaderService.hide();
		});
    };
	
	//event
	$scope.$on('$viewContentLoaded', function () {
		//TODO
	});
});