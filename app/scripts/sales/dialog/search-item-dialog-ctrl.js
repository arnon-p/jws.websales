"use strict";

angular.module('NwsFront').controller('searchItemDialogCtrl', function ($scope, $q, dateTimeUtils, documentService, dialogService, Pagination, loaderService, exceptionService) {

	//declare variable
    $scope.parentBean = null;
	$scope.dialogCondition = null;
	$scope.dialogResultList = null;
	$scope.dialogPaginable = null;
    $scope.selectedRowIndex = null;

	//declare function
	$scope.init = function () {
		$scope.dialogCondition = {};
		$scope.dialogPaginable = Pagination.getDefaultPaginable();
        $scope.dialogResultList = null;

        if($scope.modal.bean){
            $scope.parentBean = $scope.modal.bean.bean;
            $scope.selectedRowIndex = $scope.modal.bean.rowIndex;

            $scope.dialogCondition.docDate = dateTimeUtils.valueToDate($scope.parentBean.docDate);
            $scope.dialogCondition.cardCode = $scope.parentBean.cardCode;
            $scope.dialogCondition.cardName = $scope.parentBean.cardName;
            $scope.dialogCondition.priceListNum = $scope.parentBean.priceListNum;
            $scope.dialogCondition.priceListName = $scope.parentBean.priceListName;
        }
	};

	$scope.doDialogSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        $scope.dialogCondition.dialogPaginable = Pagination.getPaginable($scope.dialogPaginable, page);
        $scope.dialogCondition.paginable = $scope.dialogCondition.dialogPaginable;

        documentService.searchItemForSO($scope.dialogCondition).then(function(response) {
            deferred.resolve(response);
            $scope.dialogResultList = response.response;
            $scope.dialogPaginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.dialogResultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.doChooseRow = function(e, row){
    	//set data to parent
    	$scope.$parent.dialogChooseItem(row, $scope.selectedRowIndex);

    	dialogService.closeDialog('searchItemModal', function () {
			loaderService.hide();
		});

        // $scope.modal = {};
    };
	
	//event
	$scope.$on('$viewContentLoaded', function () {
		//TODO
	});
});