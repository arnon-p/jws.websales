"use strict";

angular.module('NwsFront').controller('searchGrossProfitDialogCtrl', function ($scope, $q, documentService, dialogService, numberUtils, loaderService, exceptionService) {

	//declare variable
	$scope.dialogCondition = null;
	$scope.dialogResultList = null;
	// $scope.dialogPaginable = null;

	//declare function
	$scope.init = function () {
		$scope.dialogCondition = {};
		// $scope.dialogPaginable = Pagination.getDefaultPaginable();
        $scope.dialogResultList = null;

        if($scope.modal.bean){
            $scope.dialogCondition.docEntry = $scope.modal.bean.docEntry;
        }

        $scope.doDialogSearch(null, 0);
	};

	$scope.doDialogSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.dialogCondition.dialogPaginable = Pagination.getPaginable($scope.dialogPaginable, page);
        $scope.dialogCondition.paginable = $scope.dialogCondition.dialogPaginable;

        documentService.searchGrossProfitByCondition($scope.dialogCondition).then(function(response) {
            deferred.resolve(response);
            $scope.dialogResultList = response.response;
            // $scope.dialogPaginable = response.paginable;
            loaderService.hide();
        }, function(error) {
            $scope.dialogResultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.doChooseRow = function(e, row){
    	//set data to parent
    	$scope.$parent.dialogChoosePartner(row);

    	dialogService.closeDialog('searchGrossProfitModal', function () {
			loaderService.hide();
		});
    };
    
    $scope.sumBasePrice = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = numberUtils.fixedDecimalPlaces(sum, 2) + numberUtils.fixedDecimalPlaces(item.totalBasePrice, 2);
        });

        return sum;
    };
    $scope.sumGrossProfit = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = numberUtils.fixedDecimalPlaces(sum, 2) + numberUtils.fixedDecimalPlaces(item.grossProfit, 2);
        });

        return sum;
    };
    $scope.sumProfitPercent = function(itemList) {
        // let sum = 0;

        // angular.forEach(itemList, function(item){
        //     sum = numberUtils.fixedDecimalPlaces(sum, 2) + numberUtils.fixedDecimalPlaces(item.profitPercent, 2);
        // });

        // sum = numberUtils.fixedDecimalPlaces(sum, 2) / numberUtils.fixedDecimalPlaces(itemList.length, 2);

        // sum = numberUtils.fixedDecimalPlaces(sum, 2);

        let sumGrossProfit = 0;
        let sumSalesPrice = 0;

        angular.forEach(itemList, function(item){
            sumGrossProfit = numberUtils.fixedDecimalPlaces(sumGrossProfit, 2) + numberUtils.fixedDecimalPlaces(item.grossProfit, 2);

            let price = numberUtils.fixedDecimalPlaces(item.salesPrice, 2) * numberUtils.fixedDecimalPlaces(item.quantity, 2);

            sumSalesPrice = numberUtils.fixedDecimalPlaces(sumSalesPrice, 2) + numberUtils.fixedDecimalPlaces(price, 2);
        });

        let sum = numberUtils.fixedDecimalPlaces(sumGrossProfit, 2) / numberUtils.fixedDecimalPlaces(sumSalesPrice, 2);
        sum = sum * 100.00;

        sum = numberUtils.fixedDecimalPlaces(sum, 2);

        return sum;
    };
    $scope.sumSalesPrice = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = numberUtils.fixedDecimalPlaces(sum, 2) + numberUtils.fixedDecimalPlaces(item.salesPrice, 2);
        });

        return sum;
    };
    $scope.sumQty = function(itemList) {
        let sum = 0;

        angular.forEach(itemList, function(item){
            sum = numberUtils.fixedDecimalPlaces(sum, 2) + numberUtils.fixedDecimalPlaces(item.quantity, 2);
        });

        return sum;
    };

	//event
	$scope.$on('$viewContentLoaded', function () {
		//TODO
	});
});