"use strict";

angular.module('NwsFront').controller('searchBatchNoDialogCtrl', function ($scope, $q, documentService, dialogService, loaderService, exceptionService) {

	//declare variable
    $scope.parentBean = null;
	$scope.dialogCondition = null;
	$scope.dialogResultList = null;
	// $scope.dialogPaginable = null;
    $scope.selectedRowIndex = null;
    $scope.selectedBatchNoList = null;

	//declare function
	$scope.init = function () {
		$scope.dialogCondition = {};
		// $scope.dialogPaginable = Pagination.getDefaultPaginable();
        $scope.dialogResultList = null;
        $scope.selectedBatchNoList = [];

        if($scope.modal.bean){
            $scope.selectedRowIndex = $scope.modal.bean.rowIndex;

            $scope.dialogCondition.itemCode = $scope.modal.bean.selectedRow.itemCode;
            $scope.dialogCondition.whsCode = $scope.modal.bean.selectedRow.warehouseCode;
            $scope.dialogCondition.quantity = $scope.modal.bean.selectedRow.quantity;
            $scope.dialogCondition.uomBaseQty = $scope.modal.bean.selectedRow.uomBaseQty;
            $scope.dialogCondition.uomAltQty = $scope.modal.bean.selectedRow.uomAltQty;

            if($scope.modal.bean.selectedRow.batchs){
                $scope.selectedBatchNoList = angular.copy($scope.modal.bean.selectedRow.batchs);
            }
        }

        $scope.doDialogSearch(null, 0);
	};

	$scope.doDialogSearch = function(e, page) {
        var deferred = $q.defer();
        loaderService.show();
        // $scope.dialogCondition.dialogPaginable = Pagination.getPaginable($scope.dialogPaginable, page);
        // $scope.dialogCondition.paginable = $scope.dialogCondition.dialogPaginable;

        documentService.searchBatchNumber($scope.dialogCondition).then(function(response) {
            deferred.resolve(response);

            $scope.dialogResultList = response.response;

            if($scope.selectedBatchNoList.length !== 0){
                let removeIndex = [];

                for(let i=0;i<response.response.length;i++){
                    let batch = response.response[i];

                    for(let j=0;j<$scope.selectedBatchNoList.length;j++){
                        let selectedBatch = $scope.selectedBatchNoList[j];

                        selectedBatch.batchQty = selectedBatch.quantity;

                        if(batch.absEntry===selectedBatch.absEntry){
                            batch.quantity = batch.quantity - selectedBatch.batchQty;

                            break;
                        }
                    }

                    if(batch.quantity===0){
                        removeIndex.push(i);
                    }
                    // let selected = false;

                    // for(let j=0;j<$scope.selectedBatchNoList.length;j++){
                    //     let selectedBatch = $scope.selectedBatchNoList[j];

                    //     if(batch.absEntry===selectedBatch.absEntry){
                    //         selected = true;

                    //         break;
                    //     }
                    // }

                    // if(!selected){
                    //     $scope.dialogResultList.push(batch);
                    // }
                }

                if(removeIndex.length>0){
                    angular.forEach(removeIndex, function(id){
                        $scope.dialogResultList.splice(id, 1);
                    });
                    
                }
            }

            // if($scope.selectedBatchNoList.length !== 0){
            //     $scope.dialogResultList = [];

            //     for(let i=0;i<response.response.length;i++){
            //         let batch = response.response[i];

            //         let selected = false;

            //         for(let j=0;j<$scope.selectedBatchNoList.length;j++){
            //             let selectedBatch = $scope.selectedBatchNoList[j];

            //             if(batch.absEntry===selectedBatch.absEntry){
            //                 selected = true;

            //                 break;
            //             }
            //         }

            //         if(!selected){
            //             $scope.dialogResultList.push(batch);
            //         }
            //     }
            // }else{
            //     $scope.dialogResultList = response.response;
            // }

            // $scope.dialogPaginable = response.paginable;

            //calculate alllocated qty
            $scope.calculateAllocatedQty();
            $scope.calculateInvQty();

            loaderService.hide();
        }, function(error) {
            $scope.dialogResultList = null;
            loaderService.hide();
            deferred.reject(error);
            exceptionService.queryHandler($scope, error);
        });
        return deferred.promise;
    };

    $scope.doAddBatch = function(e, batchIndex){
        let row = $scope.dialogResultList[batchIndex];

        if(!row.batchQty || row.batchQty>row.quantity) {
            return false;
        }

        //validate not over invQty
        if(row.batchQty>$scope.dialogCondition.invQty){
            return false;
        }

        row.lineNum = $scope.selectedRowIndex;

        row.quantity = row.quantity - row.batchQty;

        if($scope.selectedBatchNoList.length===0){
            let newObj = angular.copy(row);
            
            $scope.selectedBatchNoList.push(newObj);
        }else{
            let dup = false;

            angular.forEach($scope.selectedBatchNoList, function(selected){
                if(!dup && row.absEntry===selected.absEntry){
                    selected.batchQty = selected.batchQty + row.batchQty;

                    dup = true;
                }
            });

            if(!dup){
                let newObj = angular.copy(row);

                $scope.selectedBatchNoList.push(newObj);
            }
        }

        if(row.quantity===0){
            $scope.dialogResultList.splice(batchIndex, 1);
        }

        //calculate head data
        $scope.calculateAllocatedQty();
        $scope.calculateInvQty();
    };

    $scope.doRemoveBatch = function(e, batchIndex){
        if ($scope.selectedBatchNoList) {
            let row = $scope.selectedBatchNoList[batchIndex];

            let match = false;

            for(let i=0;i<$scope.dialogResultList.length;i++){
                let result = $scope.dialogResultList[i];

                if(row.absEntry===result.absEntry){
                    result.quantity = result.quantity + row.batchQty;

                    match = true;

                    break;
                }
            }

            if(!match){
                row.quantity = row.batchQty;

                $scope.dialogResultList.push(row);
            }

            $scope.selectedBatchNoList.splice(batchIndex, 1);
        }

        //calculate head data
        $scope.calculateAllocatedQty();
        $scope.calculateInvQty();
    };

    $scope.doSave = function(e){
        angular.forEach($scope.selectedBatchNoList, function(selected){
            selected.quantity = selected.batchQty;
        });

    	//set data to parent
    	$scope.$parent.dialogChooseBatchNo($scope.selectedBatchNoList, $scope.selectedRowIndex);

    	dialogService.closeDialog('searchBatchNoModal', function () {
			loaderService.hide();
		});

        // $scope.modal = {};
    };

    $scope.calculateInvQty = function() {
        if($scope.dialogCondition.quantity && $scope.dialogCondition.quantity > 0 && $scope.dialogCondition.uomBaseQty && $scope.dialogCondition.uomAltQty){
            $scope.dialogCondition.invQty = $scope.dialogCondition.quantity * ($scope.dialogCondition.uomBaseQty / $scope.dialogCondition.uomAltQty);

            $scope.dialogCondition.invQty = $scope.dialogCondition.invQty - $scope.dialogCondition.allocatedQty;
        }else{
            $scope.dialogCondition.invQty = 0;
        }
    };

    $scope.calculateAllocatedQty = function() {
        if($scope.selectedBatchNoList && $scope.selectedBatchNoList.length > 0){
            let result = 0;

            angular.forEach($scope.selectedBatchNoList, function(selected){

                result = result + selected.batchQty;

            });

            $scope.dialogCondition.allocatedQty = result;
        }else{
            $scope.dialogCondition.allocatedQty = 0;
        }
    };
	
	//event
	$scope.$on('$viewContentLoaded', function () {
		//TODO
	});
});