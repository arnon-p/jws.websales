"use strict";

angular.module('NwsFront').controller('homeCtrl', function ($scope, $timeout){
	

	$scope.$on('$viewContentLoaded', function() {
		$timeout(function () {
            let scope = $scope.getScopeById("loginModalCtrl");

            if(scope){
            	scope.initForm();	
            }

        }, 100);
	});

});