"use strict";

angular.module('NwsFront').constant('appConfig', {
    databaseName: 'nwsdb',
    Company: "WGE",
    uploadMaxSize: 5242880
}).constant('localStorageConfig', {
    userToken: 'nws-token',
    appTime: 'backend-time',
    loadingCacheId: 'loading-cache',
    userProfile: 'nws-profile',
    userTranslate: 'nws-translate',

    //constant
    salesEmployee: 'sales-employee',
    bpGroup: 'bp-group',
    paymentTerms: 'payment-terms',
    shippingType: 'shipping-type',
    addressType: 'address-type',
    itemGroup: 'item-group',
    whse: 'whse',
    properties: 'properties',
    country: 'country',
    taxGroup: 'tax-group',
    state: 'state',
    dimension1: 'dimension1',
    dimension2: 'dimension2',
    dimension3: 'dimension3',
    dimension4: 'dimension4',
    dimension5: 'dimension5',
    series: 'series',
    priceList: 'price-list',
    orderStatus: 'document-status',
    brand: 'brand',
    channel: 'channel',
    locationAddress: 'location-address',
    uomGroup: 'uom-group',
    userCompany: 'nws-company',
    companyList: 'company-list',
    approveStatus: 'approve-status',
    
    //business
    editBean: 'editing-bean',
    editSubBean: 'sub-editing-bean',
    editDialogBean: 'sub-editing-dialog-bean',
}).constant('appEndpoint', {
    // baseUrl: 'http://110.170.165.102/api/v1/jws'    //DEV
    // baseUrl: 'http://localhost:8080'     //LOCAL
    baseUrl: 'http://110.170.165.104:10102/api/v1/jws'    //PROD
});