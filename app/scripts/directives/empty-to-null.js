//use for convert number to text for bind with dropdown

angular.module('NwsFront').directive('emptyToNull', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        if(!val || val.length===0){
          return null;
        }

        return val;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});