//use for convert number to text for bind with dropdown
//form group class
angular.module('NwsFront').directive("datePickerGroup", function() {

  return {

    restrict: "E",
    replace: true,
    require:'ngModel',
    templateUrl: 'views/directives/date-picker.html',
    scope: {
      ngModel: '=',
      label: '@',
      ngChange: '=',
      name: '@',
      required: '@'
    },
    link: function(scope, el, attr) {
      scope.dateStr = "";
      scope.typeState = true;

      if(!scope.required){
        scope.required = false;
      }

      scope.doTypeText = function(e) {
        if(scope.dateStr && scope.dateStr.length===8){
          let date = moment(scope.dateStr, 'DDMMYYYY').toDate();

          scope.ngModel = date;

          let str = moment(scope.ngModel).format('DD.MM.YYYY');

          scope.dateStr = str;
        }else{
          scope.ngModel = null;
          scope.dateStr = null;
        }

        if(scope.ngChange){
          scope.ngChange();
        }
      };

      scope.doSelectDate = function(e) {

        if(scope.ngModel && angular.isDate(scope.ngModel)){
          let str = moment(scope.ngModel).format('DD.MM.YYYY');

          scope.dateStr = str;
        }else{
          scope.dateStr = null;
        }

        scope.typeState = true;

        if(scope.ngChange){
          scope.ngChange();
        }
      };

      //init
      scope.doSelectDate();
    }

  };

});