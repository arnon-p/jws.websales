//use for convert number to text for bind with dropdown

angular.module('NwsFront').directive('convertToBoolean', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        // return val != null ? parseInt(val, 10) : null;
        return val != null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});