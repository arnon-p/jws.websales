"use strict";
// for handle enter event
angular.module('NwsFront').directive('ngOneStep', function () {
    return function (scope, element, attrs) {
        element.bind("focusin", function (event) {
            this.beforeVal = this.value;
        });

        element.bind("change", function (event) {
            // console.log('value : ' + this.value);
            
            var newVal = this.value;
            var beforeVal = this.beforeVal;

            newVal = parseFloat(newVal);
            beforeVal = parseFloat(beforeVal);

            var diffVal = Math.round(Math.abs(newVal-beforeVal) * 100) / 100;

            // console.log('diff val = ' + diffVal);

            if(diffVal === 0.01){    //check step up/down event by 0.01
                if(newVal>beforeVal){   //step up
                    newVal = newVal - 0.01; //back step 0.01

                    newVal = newVal + 1;    //step up by 1
                }else{
                    newVal = newVal + 0.01; //back step 0.01

                    newVal = newVal - 1;    //step up by 1
                }
                
                this.value = newVal;
            }

            this.beforeVal = newVal;
        });
    };
});