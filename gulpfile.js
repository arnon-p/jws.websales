'use strict';

var appName = 'NwsFront';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var express = require('express');
var path = require('path');
var open = require('open');
var stylish = require('jshint-stylish');
var connectLr = require('connect-livereload');
var streamqueue = require('streamqueue');
var runSequence = require('run-sequence');
var merge = require('merge-stream');
var ripple = require('ripple-emulator');
var wiredep = require('wiredep');
var uglify = require('gulp-uglify-es').default;
var eslint = require('gulp-eslint');

/**
 * Parse arguments
 */
var args = require('yargs')
    .alias('e', 'emulate')
    .alias('b', 'build')
    .alias('r', 'run')
    // remove all debug messages (console.logs, alerts etc) from release build
    .alias('release', 'strip-debug')
    .default('build', false)
    .default('port', 9000)
    .default('strip-debug', false)
    .argv;

var build = !!(args.build || args.emulate || args.run);
var emulate = args.emulate;
var run = args.run;
var port = args.port;
var stripDebug = !!args.stripDebug;
var targetDir = path.resolve(build ? 'www' : 'tmp');

// if we just use emualate or run without specifying platform, we assume iOS
// in this case the value returned from yargs would just be true
if (emulate === true) {
    emulate = 'ios';
}
if (run === true) {
    run = 'ios';
}

// global error handler
var errorHandler = function(error) {
  if (build) {
    throw error;
  } else {
    // beep(2, 170);
    plugins.util.log(error);
  }
};

// clean target dir
gulp.task('clean', function(done) {
  return del([targetDir], done);
});

// precompile .scss 
gulp.task('styles', function() {

  var saasOptions = build ? { style: 'compressed' } : { style: 'expanded' };
  var lessOptions = { paths: [ path.join(__dirname, 'less', 'includes') ] };

  var wireDepOpt = {
      //exclude : ['/bootstrap/']
  };

  var sassStream = gulp.src('app/styles/**/*.scss')
    .pipe(plugins.sass(saasOptions))
    .on('error', function(err) {
      console.log('err: ', err);
    });

  var lessStream = gulp.src(['app/styles/**/*.less'])
    .pipe(plugins.less(lessOptions))
    .on('error', function(err) {
      console.log('err: ', err);
    });

  var cssStream = gulp.src('app/styles/**/*.css')
    .on('error', function(err) {
      console.log('err: ', err);
    });

  // var bowerSassStream = gulp.src(wiredep(wireDepOpt).scss)
  //   .pipe(plugins.sass(saasOptions))
  //   .on('error', function(err) {
  //     console.log('err: ', err);
  //   });

  // var bowerLessStream = gulp.src(wiredep(wireDepOpt).less)
  //   .pipe(plugins.less(lessOptions))
  //   .on('error', function(err) {
  //     console.log('err: ', err);
  //   });
  
    var bowerCssStream = gulp.src(wiredep(wireDepOpt).css)
        .on('error', function(err) {
        console.log('err: ', err);
        });

  // build bootstrap css dynamically to support custom themes
//   var bootstrapStream = gulp.src(['bower_components/bootstrap/dist/css/bootstrap.min.css', 'bower_components/bootstrap/dist/css/bootstrap-theme.min.css'])
//     //.pipe(plugins.sass(saasOptions))
//     .on('error', function(err) {
//         console.log('err: ', err);
//         beep();
//       });

    //angucomplete
    var angucompleteCss = gulp.src(['bower_components/angucomplete/angucomplete.css']).on('error', function(err) {
        console.log('err: ', err);
        });

  return streamqueue({ objectMode: true }, sassStream, lessStream, cssStream, bowerCssStream, angucompleteCss)
    .pipe(plugins.autoprefixer('last 1 Chrome version', 'last 3 iOS versions', 'last 3 Android versions'))
    .pipe(plugins.concat('main.css'))
    .pipe(plugins.if(build, plugins.stripCssComments()))
    .pipe(plugins.if(build && !emulate, plugins.rev()))
    .pipe(gulp.dest(path.join(targetDir, 'styles')))
    .on('error', errorHandler);
});

// build templatecache, copy scripts.
// if build: concat, minsafe, uglify and versionize
gulp.task('views', function() {
    var dest = path.join(targetDir, 'scripts');

  var minifyConfig = {
    collapseWhitespace: true,
    collapseBooleanAttributes: true,
    removeAttributeQuotes: true,
    removeComments: true
  };

  // prepare angular template cache from html templates
  // (remember to change appName var to desired module name)
  var templateStream = gulp
    .src('**/*.html', { cwd: 'app/views'})
    .pipe(plugins.angularTemplatecache('views.js', {
      root: 'views/',
      module: appName,
      htmlmin: build && minifyConfig
    }));

  return streamqueue({ objectMode: true }, templateStream)
    .pipe(plugins.if(build, plugins.ngAnnotate()))
    .pipe(plugins.if(stripDebug, plugins.stripDebug()))
    .pipe(plugins.if(build, plugins.concat('views.js')))
    .pipe(plugins.if(build, uglify()))
    .pipe(plugins.if(build && !emulate, plugins.rev()))

    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
});

gulp.task('scripts', function() {
  var dest = path.join(targetDir, 'scripts');

  var minifyConfig = {
    collapseWhitespace: true,
    collapseBooleanAttributes: true,
    removeAttributeQuotes: true,
    removeComments: true
  };

  var scriptStream = gulp
    .src(['!views*.js', 'app.js', '**/*.*'], { cwd: 'app/scripts' })

    .pipe(plugins.if(!build, plugins.changed(dest)));


  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.ngAnnotate()))
    .pipe(plugins.if(stripDebug, plugins.stripDebug()))
    .pipe(plugins.if(build, plugins.concat('app.js')))
    .pipe(plugins.if(build, uglify()))
    .pipe(plugins.if(build && !emulate, plugins.rev()))

    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
});

// copy fonts
gulp.task('fonts', function() {
  return gulp
//     .src(['app/fonts/*.*', 'bower_components/bootstrap/dist/fonts/*.*'])
        .src(['app/fonts/*.*'])

    .pipe(gulp.dest(path.join(targetDir, 'fonts')))

    .on('error', errorHandler);
});

//font-awesome only
gulp.task('fontawesome', function() {
  var fontSrc = ['bower_components/components-font-awesome/fonts/**/*.*'];
    
  return gulp.src(fontSrc)

    .pipe(gulp.dest(path.join(targetDir, 'fonts')))

    .on('error', errorHandler);
});

//all theme assets files
gulp.task('assets', function() {
  var assetsSrc = ['app/assets/**/*.*'];
    
  return gulp.src(assetsSrc)

    .pipe(gulp.dest(path.join(targetDir, 'assets')))

    .on('error', errorHandler);
});

//translate .json
gulp.task('translation', function() {
  var translateSrc = ['app/translation/**/*.json'];
    
  return gulp.src(translateSrc)

    .pipe(gulp.dest(path.join(targetDir, 'translation')))

    .on('error', errorHandler);
});

// copy images
gulp.task('images', function() {
  return gulp.src('app/images/**/*.*')
    .pipe(gulp.dest(path.join(targetDir, 'images')))

    .on('error', errorHandler);
});

// copy sounds
gulp.task('sounds', function() {
  return gulp.src('app/sounds/**/*.*')
    .pipe(gulp.dest(path.join(targetDir, 'sounds')))

    .on('error', errorHandler);
});

// lint js sources based on .jshintrc ruleset
gulp.task('jsHint', function(done) {
  return gulp
    .src('app/scripts/**/*.js')
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter(stylish))

    .on('error', errorHandler);
    done();
});

// concatenate and minify vendor sources
gulp.task('vendor', function() {
  var wireDepOpt = {
      //exclude : ['/bootstrap/']
  };

  var vendorFiles = wiredep(wireDepOpt).js;

  //console.log(vendorFiles);

  return gulp.src(vendorFiles)
    .pipe(plugins.concat('vendor.js'))
    .pipe(plugins.if(build, plugins.uglify()))
    .pipe(plugins.if(build, plugins.rev()))

    .pipe(gulp.dest(targetDir))

    .on('error', errorHandler);
});

// inject the files in index.html
gulp.task('index', ['scripts', 'views'], function() {

  // build has a '-versionnumber' suffix
  var cssNaming = 'styles/main*';

  // injects 'src' into index.html at position 'tag'
  var _inject = function(src, tag) {
    return plugins.inject(src, {
      starttag: '<!-- inject:' + tag + ':{{ext}} -->',
      addRootSlash: false
    });
  };

  // get all our javascript sources
  // in development mode, it's better to add each file seperately.
  // it makes debugging easier.
  var _getAllScriptSources = function() {
    var scriptStream = gulp.src(['scripts/app.js', 'scripts/**/*.js'], { cwd: targetDir });
    return streamqueue({ objectMode: true }, scriptStream);
  };

  return gulp.src('app/index.html')
    // inject css
    .pipe(_inject(gulp.src(cssNaming, { cwd: targetDir, read: false }), 'app-styles'))
    // inject vendor.js
    .pipe(_inject(gulp.src('vendor*.js', { cwd: targetDir, read: false }), 'vendor'))
    // inject app.js (build) or all js files indivually (dev)
    .pipe(plugins.if(build,
      _inject(gulp.src('scripts/app*.js', { cwd: targetDir, read: false }), 'app'),
      _inject(_getAllScriptSources(), 'app')
    ))
    .pipe(_inject(gulp.src('scripts/views*.js', { cwd: targetDir, read: false }), 'views'))
    .pipe(plugins.if(build, plugins.injectString.replace('<!-- inject:cordova:js --><!-- endinject -->', '<script type="text/javascript" src="cordova.js"></script>')))
    .pipe(gulp.dest(targetDir))
    .on('error', errorHandler);
});

gulp.task('lint', function() {
  return gulp.src(['app/scripts/**/*.js'])
      .pipe(eslint({
        useEslintrc: true
      }))
      .pipe(eslint.results(results => {
          console.log(`Total Results: ${results.length}`);
          console.log(`Total Warnings: ${results.warningCount}`);
          console.log(`Total Errors: ${results.errorCount}`);
      }))
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
});

// start local express server
gulp.task('serve', function() {
  express()
    .use(!build ? connectLr() : function(){})
    .use(express.static(targetDir))
    .listen(port);
  open('http://localhost:' + port + '/');
});

// select emulator device
// gulp.task('select', plugins.shell.task([
//   './helpers/emulateios'
// ]));

// ripple emulator
gulp.task('ripple', ['scripts', 'styles', 'watchers'], function() {

  var options = {
    keepAlive: false,
    open: true,
    port: 4400
  };

  // Start the ripple server
  ripple.emulate.start(options);

  open('http://localhost:' + options.port + '?enableripple=true');
});

// start watchers
gulp.task('watchers', function() {
  plugins.livereload.listen();
  gulp.watch('app/styles/**/*.*', ['styles']);
  gulp.watch('app/fonts/**', ['fonts']);
  gulp.watch('app/translation/**/*.json', ['translation']);
  gulp.watch('app/images/**/*.*', ['images']);
  gulp.watch('app/assets/**/*.*', ['assets']);
  gulp.watch('app/scripts/**/*.js', ['index']);
  gulp.watch('./bower.json', ['vendor']);
  gulp.watch('app/views/**/*.html', ['index']);
  gulp.watch('app/index.html', ['index']);
  gulp.watch(targetDir + '/**')
    .on('change', plugins.livereload.changed)
    .on('error', errorHandler);
});

// no-op = empty function
gulp.task('noop', function() {});

// our main sequence, with some conditional jobs depending on params
gulp.task('default', function(done) {
  runSequence(
    'clean',
    [
      'fonts',
      'assets',
//       'fontawesome',
      'translation',
      'styles',
      'images',
//       'sounds',
      'vendor'
    ],
    'index',
    build ? 'noop' : 'watchers',
    build ? 'noop' : 'serve',
    emulate ? ['watchers'] : 'noop',
    done);
});